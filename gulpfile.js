var gulp = require('gulp');
var del = require('del');
var browserSync = require('browser-sync');
var concat = require('gulp-concat');
var coffeeify = require('gulp-coffeeify');
var less = require('gulp-less');
var minifycss = require('gulp-minify-css');
var uglify = require('gulp-uglify');
var reload = browserSync.reload;
var plumber = require('gulp-plumber');
var notify = require('gulp-notify');
var cached = require('gulp-cached');
var merge = require('merge-stream');
var Config = require('./gulpConfig.js');

//实时刷新
gulp.task('serve', function() {
  browserSync({
    proxy: {
        target: Config.url,
    }
  });
  gulp.watch(['views/**/*.blade.php', 'assets/**/**.coffee', 'assets/**/**.less'], {cwd: 'resources'}, reload);
});


//编译coffee
gulp.task('coffee', function(){
    var components = gulp.src([
        
    ])
    .pipe(concat('components.coffee'))
    .pipe(gulp.dest('./resources/assets/components'));

    var adminComponents = gulp.src([
        './resources/assets/components/admin/header/index.coffee'
    ])
    .pipe(concat('admin-components.coffee'))
    .pipe(gulp.dest('./resources/assets/components/admin'));

    var common =  gulp.src([
        './resources/assets/common/common.coffee'
    ])
    .pipe(concat('common.coffee'))
    .pipe(gulp.dest('./resources/assets/common'));

    var adminCommon =  gulp.src([
        './resources/assets/common/admin/common.coffee'
    ])
    .pipe(concat('admin-common.coffee'))
    .pipe(gulp.dest('./resources/assets/common/admin'));

    var pages =  gulp.src([
        './resources/assets/**/**.coffee',
    ])
    .pipe(cached('coffeecache'))
    .pipe(coffeeify())
    .pipe(gulp.dest('./public/js'));

    return merge([components, adminComponents, common, adminCommon, pages]);
});

//编译less
gulp.task('less', function(){
    var components = gulp.src([
        './resources/assets/components/footer/style.less',
        './resources/assets/components/header/style.less',
    ])
    .pipe(cached('lesscache'))
    .pipe(concat('components.less'))
    .pipe(plumber({errorHandler: notify.onError('Error: <%= error.message %>')}))
    .pipe(less())
    .pipe(gulp.dest('./public/css/components'));

    var common = gulp.src([
        './resources/assets/common/common.less'
    ])
    .pipe(cached('lesscache'))
    .pipe(concat('common.less'))
    .pipe(plumber({errorHandler: notify.onError('Error: <%= error.message %>')}))
    .pipe(less())
    .pipe(gulp.dest('./public/css/common'));

    var pages =  gulp.src([
        './resources/assets/pages/**/**.less'
    ])
    .pipe(cached('lesscache'))
    .pipe(plumber({errorHandler: notify.onError('Error: <%= error.message %>')}))
    .pipe(less())
    .pipe(gulp.dest('./public/css/pages'));

    return merge([components, common, pages]);
});


//压缩js文件
gulp.task('uglify', ['coffee'], function(){
    var jsComponentsUglify =  
        gulp.src('./public/js/components.js')
            .pipe(uglify())
            .pipe(gulp.dest('./public/js/'));
     var jsCommonUglify =  
        gulp.src('./public/js/common.js')
            .pipe(uglify())
            .pipe(gulp.dest('./public/js/'));
    var jsPagesUglify = 
        gulp.src('./public/js/pages/**')
            .pipe(uglify())
            .pipe(gulp.dest('./public/js/pages/'));
    return merge([jsComponentsUglify, jsCommonUglify, jsPagesUglify]);
});

//压缩css文件
gulp.task('minifycss', ['less'], function(){
    var cssComponentsMinifycss =  
        gulp.src('./public/css/components.css')
            .pipe(minifycss())
            .pipe(gulp.dest('./public/css/'));
     var cssCommonMinifycss =  
        gulp.src('./public/css/common.css')
            .pipe(minifycss())
            .pipe(gulp.dest('./public/css/'));
    var cssPageMinifycss = 
        gulp.src('./public/css/pages/**')
            .pipe(minifycss())
            .pipe(gulp.dest('./public/css/pages/'));
    return merge([cssComponentsMinifycss, cssCommonMinifycss, cssPageMinifycss]);
});


gulp.task('clean', function(cb) {
    del([
        './public/css/**',
        './public/js/**',
    ], cb);
});


gulp.task('default', ['clean','coffee', 'less']);
gulp.task('prod', ['clean','coffee', 'less', 'uglify', 'minifycss']);

gulp.task('watch', function(){
    gulp.watch([
        './resources/assets/**/**.coffee',
        './resources/assets/**/**.less'
    ], ['coffee', 'less']); 
});


