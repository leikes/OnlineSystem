<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('uname')->unique();//用户名
            $table->string('name');
            $table->string('password', 60);//密码
            $table->integer('entrance_id');//入口id
            $table->integer('role_id');//角色id
            $table->rememberToken();
            $table->timestamps();

            // $table->foreign('entrance_id')->references('id')->on('entrances');//外鍵關聯入口表

            // $table->foreign('role_id')->references('id')->on('roles');
            //外鍵關聯角色表
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
