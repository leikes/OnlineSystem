<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');//姓名
            $table->string('phone');//电话
            $table->integer('age')->nullable();//年龄
            $table->boolean('sex');//性别
            $table->date('order_date')->nullable();//预约日期
            $table->date('arrival_date')->nullable();//到诊日期
            $table->string('register_type');//登记类型
            $table->string('order_number');//预约号
            $table->boolean('is_ready');//是否到诊
            $table->string('doctor');//指定专家
            $table->string('user_name');//用户id
            $table->string('disease_type');//疾病类型
            $table->integer('province_id');//省份
            $table->integer('city_id');//城市
            $table->string('address');//详细地址
            $table->string('channel');//咨询渠道
            $table->string('keyword')->nullable();//搜索关键词
            $table->string('remarks')->nullable();//备注
            $table->text('record')->nullable();//聊天记录
            $table->date('visit_date');//回访日期
            $table->string('qq')->nullable();//qq
            $table->string('wechat')->nullable();//微信
            $table->string('visit_info')->nullable();//回访信息
            $table->integer('entrance_id');//所属端口id
            $table->timestamps();

            // $table->foreign('entrance_id')->references('id')->on('entrances');

            // $table->foreign('province_id')->references('id')->on('provinces');

            // $table->foreign('city_id')->references('id')->on('citys');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('orders');
    }
}
