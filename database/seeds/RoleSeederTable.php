<?php

use Illuminate\Database\Seeder;
use App\Models\Role;

class RoleSeederTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create([
        	'role_name' => '超级管理员',
        	]);

        Role::create([
        	'role_name' => '管理员',
        	]);

        Role::create([
        	'role_name' => '咨询组',
        	]);

        Role::create([
            'role_name' => '竞价组',
            ]);
    }
}
