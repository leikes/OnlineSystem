<?php

use Illuminate\Database\Seeder;
use App\Models\Channel;
class ChannelSeederTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Channel::create([
        	'channel_name' => 'qq',
    		'entrance_id' => 2
        	]);

        Channel::create([
        	'channel_name' => '微信',
    		'entrance_id' => 2
        	]);

        Channel::create([
        	'channel_name' => '商务通',
    		'entrance_id' => 3
        	]);

        Channel::create([
        	'channel_name' => '电话',
    		'entrance_id' => 3
        	]);

    }
}
