<?php

use Illuminate\Database\Seeder;
use App\Models\Disease;

class DiseaseSeederTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Disease::create([
        	'disease_name' => '糖尿病',
        	'entrance_id' => 2
        	]);

        Disease::create([
        	'disease_name' => '肥胖',
        	'entrance_id' => 2
        	]);

        Disease::create([
        	'disease_name' => '甲亢',
        	'entrance_id' => 3
        	]);

        Disease::create([
        	'disease_name' => '乳腺囊肿',
        	'entrance_id' => 3
        	]);

    }
}
