<?php

use Illuminate\Database\Seeder;
use App\Models\Register;

class RegisterSeederTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Register::create([
        	'register_name' => '电话',
        	'entrance_id' => 2
        	]);

        Register::create([
        	'register_name' => '微信',
        	'entrance_id' => 2
        	]);

        Register::create([
        	'register_name' => 'qq',
        	'entrance_id' => 3
        	]);

        Register::create([
        	'register_name' => '义诊',
        	'entrance_id' => 3
        	]);
    }
}
