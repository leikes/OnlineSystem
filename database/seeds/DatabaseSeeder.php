<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(EntranceSeederTable::class);
        $this->call(RoleSeederTable::class);
        $this->call(UserSeederTable::class);
        $this->call(ProvinceSeederTable::class);
        $this->call(CitySeederTable::class);
        $this->call(RegisterSeederTable::class);
        $this->call(DiseaseSeederTable::class);
        $this->call(DoctorSeederTable::class);
        $this->call(ChannelSeederTable::class);
        $this->call(OrderSeederTable::class);
        Model::reguard();
    }
}
