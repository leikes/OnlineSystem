<?php

use Illuminate\Database\Seeder;
use App\Models\Order;
class OrderSeederTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Order::create([
        		'name' => '周小姐',
		    	'phone' => '13923720797',
		    	'age' => '34',
		    	'sex' => false,
		    	'order_date' => '2017-02-13',
		    	'arrival_date' => '2017-05-05',
		    	'register_type' => '电话',
		    	'order_number' => 'E5446',
		    	'is_ready' => false,
		    	'doctor' => '戴小江',
		    	'user_name' => '张威',
		    	'disease_type' => '糖尿病',
		    	'province_id' => 1,
		    	'city_id' => 1,
		    	'channel' => '微信',
		    	'keyword' => null,
		    	'remarks' => null,
		    	'record' => null,
		    	'visit_date' => '2017-03-03',
		    	'visit_info' => null,
		    	'qq' => '123456',
		    	'wechat' => 'wechat',
		    	'entrance_id' => 2
        	]);

        Order::create([
        		'name' => '饶世杰',
		    	'phone' => '13535843188',
		    	'age' => '21',
		    	'sex' => true,
		    	'order_date' => '2017-02-14',
		    	'arrival_date' => '2017-05-06',
		    	'register_type' => '电话',
		    	'order_number' => 'E5446',
		    	'is_ready' => false,
		    	'doctor' => '戴小江',
		    	'user_name' => '袁世芳',
		    	'disease_type' => '糖尿病',
		    	'province_id' => 1,
		    	'city_id' => 1,
		    	'channel' => '微信',
		    	'keyword' => null,
		    	'remarks' => null,
		    	'record' => null,
		    	'visit_date' => '2017-03-04',
		    	'visit_info' => '',
		    	'qq' => '123456',
		    	'wechat' => 'wechat',
		    	'entrance_id' => 2
        	]);
    }
}
