<?php

use Illuminate\Database\Seeder;
use App\Models\User;

class UserSeederTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
        	'uname' => 'admin01',
        	'password' => bcrypt('admin01'),
            'name' => '方健鹏',
        	'entrance_id' => 1,
        	'role_id' => 1
        	]);

        User::create([
        	'uname' => 'zixun01',
        	'password' => bcrypt('zixun01'),
            'name' => '张威',
        	'entrance_id' => 2,
        	'role_id' => 2
        	]);

        User::create([
        	'uname' => 'zixun02',
        	'password' => bcrypt('zixun02'),
            'name' => '袁世芳',
        	'entrance_id' => 2,
        	'role_id' => 3
        	]);

        User::create([
        	'uname' => 'zixun03',
        	'password' => bcrypt('zixun03'),
            'name' => '陈美德',
        	'entrance_id' => 2,
        	'role_id' => 4
        	]);

        User::create([
        	'uname' => 'zixun11',
        	'password' => bcrypt('zixun11'),
            'name' => '程晶燕',
        	'entrance_id' => 3,
        	'role_id' => 2
        	]);

        User::create([
        	'uname' => 'zixun12',
        	'password' => bcrypt('zixun12'),
            'name' => '吴嘉庆',
        	'entrance_id' => 3,
        	'role_id' => 3
        	]);

        User::create([
        	'uname' => 'zixun13',
        	'password' => bcrypt('zixun13'),
            'name' => '陈美德',
        	'entrance_id' => 3,
        	'role_id' => 4
        	]);
    }
}
