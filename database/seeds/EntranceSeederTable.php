<?php

use Illuminate\Database\Seeder;
use App\Models\Entrance;


class EntranceSeederTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Entrance::create([
        	'entrance_name' => '管理端口'
        	]);

        Entrance::create([
        	'entrance_name' => '减重端口'
        	]);

        Entrance::create([
        	'entrance_name' => '甲乳端口'
        	]);
    }
}
