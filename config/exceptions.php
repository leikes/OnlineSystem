<?php 
return [
	'verification' => [
		'parameterInvalid'=>['message'=>'字段校验异常','errCode'=>10001]
	],
	'data' => [
		'create' => ['message'=>'数据插入异常','errCode' => 20000]
	],
	'role' => [
		'role' => ['message'=>'权限不足','errCode' => 30000]
	]
];