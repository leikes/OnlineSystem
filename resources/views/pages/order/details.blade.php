@extends("layouts.admin.master")
@section("css")
		@parent
@stop

@section('js')
		@parent
		<script type="text/javascript" src="/js/pages/order/details.js"></script>
@stop

		
@section("body")
@include("components.left-nav")
<div class="master_div">
	<div class="input_box">
		<div class="box_title">
			基本信息
		</div>
		<div class="box_content">
			<div class="input_one">
				<div class="label">姓名：</div>
				<input id="name" type="text" name="" readonly="readonly" value="{{$order['name']}}">
			</div>
			<div class="input_one">
				<div class="label">电话：</div>
				<input id="phone" type="text" name="" readonly="readonly" value="{{$order['phone']}}">
			</div>
			<div class="input_one">
				<div class="label">预约日期：</div>
				<input id="order_date" type="date" name="" readonly="readonly" value="{{$order['order_date']}}">
			</div>
			<div class="input_one">
				<div class="label">咨询渠道：</div>
				<input id="order_date" type="text" name="" readonly="readonly" value="{{$order['channel']}}">
			</div>
			<div class="input_one">
				<div class="label">预约号：</div>
				<input id="order_number" type="text" name="" readonly="readonly" value="{{$order['order_number']}}">
			</div>
			<div class="input_one">
				<div class="label">预约专家：</div>
				<input id="order_number" type="text" name="" readonly="readonly" value="{{$order['doctor']}}">
			</div>
			<div class="input_one">
				<div class="label">到诊日期：</div>
				<input id="arrival_date" type="date" name="" readonly="readonly" value="{{$order['arrival_date']}}">
			</div>
			<div class="input_one">
				<div class="label">是否到诊：</div>
				<input id="order_number" type="text" name="" readonly="readonly" value="{{$order['is_ready']?'已到诊':'未到诊'}}">
			</div>
			<div class="input_one">
				<div class="label">qq：</div>
				<input id="qq" type="text" name="" readonly="readonly" value="{{$order['qq']}}">
			</div>
			<div class="input_one">
				<div class="label">微信：</div>
				<input id="wechat" type="text" name="" readonly="readonly" value="{{$order['wechat']}}">
			</div>
		</div>
	</div>
	<div class="input_box">
		<div class="box_title">
			详细信息
		</div>
		<div class="box_content">
			<div class="input_one">
				<div class="label">年龄：</div>
				<input id="age" type="text" name="" readonly="readonly" value="{{$order['age']}}">
			</div>
			<div class="input_one">
				<div class="label">性别：</div>
				<input id="age" type="text" name="" readonly="readonly" value="{{$order['sex']?'男':'女'}}">
			</div>
			<div class="input_one">
				<div class="label">疾病类型：</div>
				<input id="age" type="text" name="" readonly="readonly" value="{{$order['disease_type']}}">
			</div>
			<div class="input_one">
				<div class="label">登记类型：</div>
				<input id="age" type="text" name="" readonly="readonly" value="{{$order['register_type']}}">
			</div>
			<div class="input_one">
				<div class="label">所在省份：</div>
				<input id="age" type="text" name="" readonly="readonly" value="{{$order['province']['province_name']}}">
			</div>
			<div class="input_one">
				<div class="label">所在城市：</div>
				<input id="age" type="text" name="" readonly="readonly" value="{{$order['city']['city_name']}}">
			</div>
			<div class="input_one">
				<div class="label">详细地址：</div>
				<input id="age" type="text" name="" readonly="readonly" value="{{$order['address']}}">
			</div>
			<div class="input_one">
				<div class="label">回访日期：</div>
				<input id="visit_date" type="date" name="" readonly="readonly" value="{{$order['visit_date']}}">
			</div>
			<div class="input_one">
				<div class="label">咨询员：</div>
				<input id="visit_date" type="text" name="" readonly="readonly" value="{{$order['user_name']}}">
			</div>
			<div class="input_one">
				<div class="label">搜索关键词：</div>
				<input id="keyword" type="text" name="" readonly="readonly" value="{{$order['keyword']}}">
			</div>
			<div class="input_one">
				<div class="label">备注：</div>
				<textarea id="remarks" readonly="readonly">{{$order['remarks']}}</textarea>
			</div>
			<div class="input_one">
				<div class="label">聊天记录：</div>
				<textarea id="record" readonly="readonly">{{$order['record']}}</textarea>
			</div>
			<div class="input_one">
				<div class="label">个案接诊记录：</div>
				<textarea id="clinical" readonly="readonly">{{$order['clinical']}}</textarea>
			</div>
			<div class="input_one">
				<div class="label">个案后续跟踪记录：</div>
				<textarea id="visit_info" readonly="readonly">{{$order['visit_info']}}</textarea>
			</div>
		</div>
	</div>
	<div class="hide_div">
		@if(Auth::user()->role->role_name == "超级管理员")
			<input id="update_status" type="hidden" name="" value="1">
		@else
			@if(Auth::user()->role->role_name == "管理员")
				<input id="update_status" type="hidden" name="" value="1">
			@else
				@if(Auth::user()->name == $order['user_name'])
					<input id="update_status" type="hidden" name="" value="2">
				@else
					<input id="update_status" type="hidden" name="" value="0">
				@endif
			@endif
		@endif
		<input id="order_id" type="hidden" name="" value="{{$order['id']}}">
	</div>
	<div class="input_box">
		<div class="box_title">
			操作
		</div>
		<div class="btn_div">
			<div id="update_btn" class="confirm_btn">
				修改
			</div>
			<div id="cancel_btn" class="cancel_btn">
				取消
			</div>
		</div>
	</div>
</div>
@stop
