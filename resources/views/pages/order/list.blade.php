@extends("layouts.admin.master")
@section("css")
	@parent
@stop

@section('js')
	@parent
	<script type="text/javascript" src="/js/pages/order/list.js"></script>
	<script type="text/javascript" src="/js/pages/page.js"></script>
@stop

		
@section("body")
@include("components.left-nav")
	<div class="master_div">
		<div class="input_box">
			<div class="box_title">
				筛选条件
			</div>
			<div class="screen_div">
				<select id="search_select">
					<option value="name" 
						@if(array_key_exists('name',$conditions))
						selected
						@endif
					>姓名</option>
					<option value="phone"
						@if(array_key_exists('phone',$conditions))
						selected
						@endif
					>电话</option>
					<option value="channel"
						@if(array_key_exists('channel',$conditions))
						selected
						@endif
					>咨询渠道</option>
					<option value="user_name"
						@if(array_key_exists('user_name',$conditions))
						selected
						@endif
					>咨询员</option>
				</select>
				<input id="search_input" type="text" name=""
					@if(array_key_exists('name',$conditions))
						value="{{$conditions['name']}}"
					@elseif(array_key_exists('phone',$conditions))
						value="{{$conditions['phone']}}"
					@elseif(array_key_exists('channel',$conditions))
						value="{{$conditions['channel']}}"
					@elseif(array_key_exists('user_name',$conditions))
						value="{{$conditions['user_name']}}"
					@endif
				>
				<button class="search_btn sech_btn">搜索</button>
				<div class="label">是否到诊：</div>
				<select id="is_ready" class="sel_two">
					<option value="">不选择</option>
					<option value="1"
						@if(array_key_exists('is_ready',$conditions) && $conditions['is_ready'] ==1)
							selected
						@endif
					>已到诊</option>
					<option value="0"
						@if(array_key_exists('is_ready',$conditions) && $conditions['is_ready'] ==0)
							selected
						@endif
					>未到诊</option>
				</select>
				<div class="label">回访日期：</div>
				<input id="visit_date" type="date" name=""
					@if(array_key_exists('visit_date',$conditions))
						value="{{$conditions['visit_date']}}"
					@endif
				>
				<div class="label">所在区域：</div>
				<div class="label">省份：</div>
				<select id="province_id" class="sel_two">
					<option value="">--请选择--</option>
					@foreach($provinces as $province)
					<option value="{{ $province['id'] }}" 
						@if(array_key_exists('province_id',$conditions) && $conditions['province_id'] == $province['id'])
							selected="selected" 
						@endif
					>{{$province['province_name']}}</option>
					@endforeach
				</select>
				<div class="label">城市：</div>
				<select id="city_id" class="sel_two">
					<option value="">--请选择--</option>
					@foreach($citys as $city)
					<option value="{{ $city['id'] }}" 
						@if(array_key_exists('city_id',$conditions) && $conditions['city_id'] == $city['id'])
							selected="selected" 
						@endif
					>{{$city['city_name']}}</option>
					@endforeach
				</select>
				<div class="label">详细地址：</div>
				<input id="address" type="text" name="" 
					@if(array_key_exists('address',$conditions))
						value="{{ $conditions['address'] }}"
					@endif 
				>
				@if(Auth::user()->role->id == 1 || Auth::user()->role->id == 2)
				<button id="export_btn" class="export_btn">导出EXCEL</button>
				@endif
			</div>
		</div>
		<div class="input_box">
			<div class="box_title">
				预约信息
			</div>
			<div class="table_div">
				<table class="order_table">
					<thead>
						<tr>
							<th>编号</th>
							<th>姓名</th>
							<th>电话</th>
							<th>是否到诊</th>
							<th>省份</th>
							<th>城市</th>
							<th>详细地址</th>
							<th>咨询渠道</th>
							<th>回访日期</th>
							<th>预约日期</th>
							<th>到诊日期</th>
							<th>咨询员</th>
							<th>登记时间</th>
							<th>操作</th>
						</tr>
					</thead>
					<tbody id="list_content">
						@foreach($orders as $order)
						<tr>
							<td>{{$order['id']}}</td>
							<td>{{$order['name']}}</td>
							<td>{{$order['phone']}}</td>
							<td>{{$order['is_ready']?"已到诊":"未到诊"}}</td>
							<td>{{$order['province']['province_name']}}</td>
							<td>{{$order['city']['city_name']}}</td>
							<td>{{$order['address']}}</td>
							<td>{{$order['channel']}}</td>
							<td>{{$order['visit_date']}}</td>
							<td>{{$order['order_date']}}</td>
							<td>{{$order['arrival_date']}}</td>
							<td>{{$order['user_name']}}</td>
							<td>{{$order['created_at']}}</td>
							<td><a href="/order/details?id={{$order['id']}}">详情</a></td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>  
		</div>
		<div class="input_box">
			<div class="box_title">
				分页信息
			</div>
			<div class="pages_div">
				<div id="prePagination" class="next_pre_page">
					<a href="{{$orders->previousPageUrl()}}">&lt;</a>
				</div>
				<ul id="pagination">
					@for($i=1;$i<=ceil($orders->total()/$orders->perPage());$i++)
						<li style="display: none;" 
							@if($orders->currentPage() == $i)
								class="active"
							@endif
						><a href="{{$orders->url($i)}}">{{$i}}</a></li>
					@endfor
				</ul>
				<div id="nextPagination" class="next_pre_page">
					<a href="{{$orders->nextPageUrl()}}">&gt;</a>
				</div>
				<div class="total_page">
					共{{$orders->total()}}条记录
				</div>
			</div>
			<div class="page_hide">
				<!-- 数据总数 -->
				<input id="totle_page" type="hidden" name="" value="{{$orders->total()}}">
				<!-- 每页显示数据条数 -->
				<input id="per_page" type="hidden" name="" value="{{$orders->perPage()}}">
				<!-- 当前页 -->
				<input id="current_page" type="hidden" name="" value="{{$orders->currentPage()}}">

				<input id="previous_page_url" type="hidden" name="" value="{{$orders->previousPageUrl()}}">

				<input id="next_page_url" type="hidden" name="" value="{{$orders->nextPageUrl()}}">
		    </div>
		</div>
	</div>
@stop
