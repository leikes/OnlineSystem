@extends("layouts.admin.master")
@section("css")
		@parent
@stop

@section('js')
		@parent
		<script type="text/javascript" src="/js/pages/order/create.js"></script>
@stop

		
@section("body")
@include("components.left-nav")
<div class="master_div">
	<div class="input_box">
		<div class="box_title">
			基本信息
		</div>
		<div class="box_content">
			<div class="input_one">
				<div class="label">姓名：</div>
				<input id="name" type="text" name="">
			</div>
			<div class="input_one">
				<div class="label">电话：</div>
				<input id="phone" type="text" name="">
			</div>
			<div class="input_one">
				<div class="label">预约日期：</div>
				<input id="order_date" type="date" name="">
			</div>
			<div class="input_one">
				<div class="label">咨询渠道：</div>
				<select id="channel">
					@foreach($channels as $channel)
					<option value="{{$channel['channel_name']}}">{{$channel['channel_name']}}</option>
					@endforeach
				</select>
			</div>
			<div class="input_one">
				<div class="label">预约号：</div>
				<input id="order_number" type="text" name="" readonly="readonly" value="{{$orderNumber}}">
			</div>
			<div class="input_one">
				<div class="label">预约专家：</div>
				<select id="doctor">
					@foreach($doctors as $doctor)
					<option value="{{$doctor['doctor_name']}}">{{$doctor['doctor_name']}}</option>
					@endforeach
				</select>
			</div>
			<div class="input_one">
				<div class="label">到诊日期：</div>
				<input id="arrival_date" type="date" name="">
			</div>
			<div class="input_one">
				<div class="label">是否到诊：</div>
				<select id="is_ready">
					<option value="1">已到诊</option>
					<option value="0">未到诊</option>				
				</select>
			</div>
			<div class="input_one">
				<div class="label">qq：</div>
				<input id="qq" type="text" name="">
			</div>
			<div class="input_one">
				<div class="label">微信：</div>
				<input id="wechat" type="text" name="">
			</div>
		</div>
	</div>
	<div class="input_box">
		<div class="box_title">
			详细信息
		</div>
		<div class="box_content">
			<div class="input_one">
				<div class="label">年龄：</div>
				<input id="age" type="text" name="">
			</div>
			<div class="input_one">
				<div class="label">性别：</div>
				<select id="sex">
					<option value="1">男</option>
					<option value="0">女</option>
				</select>
			</div>
			<div class="input_one">
				<div class="label">疾病类型：</div>
				<select id="disease_type">
					@foreach($diseases as $disease)
						<option value="{{$disease['disease_name']}}">{{$disease['disease_name']}}</option>
					@endforeach
				</select>
			</div>
			<div class="input_one">
				<div class="label">登记类型：</div>
				<select id="register_type">
					@foreach($registers as $register)
						<option value="{{$register['register_name']}}">{{$register['register_name']}}</option>
					@endforeach
				</select>
			</div>
			<div class="input_one">
				<div class="label">所在省份：</div>
				<select id="province">
					<option value="1">--请选择--</option>
					@foreach($provinces as $province)
						<option value="{{$province['id']}}" >{{$province['province_name']}}</option>
					@endforeach
				</select>
			</div>
			<div class="input_one">
				<div class="label">所在城市：</div>
				<select id='city'>
					<option value="">--请选择--</option>
				</select>
			</div>
			<div class="input_one">
				<div class="label">详细地址：</div>
				<input id="address" type="text" name="">
			</div>
			<div class="input_one">
				<div class="label">回访日期：</div>
				<input id="visit_date" type="date" name="">
			</div>
			<div class="input_one">
				<div class="label">搜索关键词：</div>
				<input id="keyword" type="text" name="">
			</div>
			<br>
			<div class="input_one">
				<div class="label">备注：</div>
				<textarea id="remarks"></textarea>
			</div>
			<div class="input_one">
				<div class="label">聊天记录：</div>
				<textarea id="record"></textarea>
			</div>
			<div class="input_one">
				<div class="label">个案接诊记录：</div>
				<textarea id="clinical"></textarea>
			</div>
			<div class="input_one">
				<div class="label">个案后续跟踪记录：</div>
				<textarea id="visit_info"></textarea>
			</div>
		</div>
	</div>
	<div class="input_box">
		<div class="box_title">
			操作
		</div>
		<div class="btn_div">
			<div class="confirm_btn">
				确定
			</div>
			<div class="cancel_btn">
				取消
			</div>
		</div>
	</div>
</div>
@stop
