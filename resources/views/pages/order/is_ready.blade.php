@extends("layouts.admin.master")
@section("css")
	@parent
@stop

@section('js')
	@parent
	<script type="text/javascript" src="/js/pages/order/is_ready.js"></script>
@stop

		
@section("body")
@include("components.left-nav")
<div class="master_div">
	<div class="input_box">
		<div class="box_title">
			到诊日期段筛选
		</div>
		<div class="screen_div date_box">
			<div class="label">预计到诊时间段从&nbsp;&nbsp;</div>
			<input id="start_date" type="date" name="" value="{{$param['startDate']}}">
			<div class="label">到&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
			<input id="end_date" type="date" name="" value="{{$param['endDate']}}">
		</div>
	</div>
	<div class="input_box">
		<div class="box_title">
			到诊时间段筛选
		</div>
		<div class="totle_div">
			<button class="day">今日到诊</button>
			<button class="pre_day">昨日到诊</button>
			<button class="week">本周到诊</button>
			<button class="pre_week">上周到诊</button>
			<button class="mouth">本月到诊</button>
			<button class="pre_mouth">上月到诊</button>
		</div>
	</div>
	<div class="input_box">
		<div class="box_title">
			到诊列表
		</div>
		<div class="table_div">
			<table class="order_table">
				<thead>
					<tr>
			          <th>编号</th>
			          <th>姓名</th>
			          <th>电话</th>
			          <th>预约号</th>
			          <th>是否到诊</th>
			          <th>省份</th>
			          <th>城市</th>
			          <th>咨询渠道</th>
			          <th>回访日期</th>
			          <th>预约日期</th>
			          <th>到诊日期</th>
			          <th>咨询员</th>
			          <th>登记时间</th>
			          <th>操作</th>
			        </tr>
				</thead>
				<tbody>
					@foreach($orders as $order)
					<tr>
						<td>{{$order->id}}</td>
						<td>{{$order->name}}</td>
						<td>{{$order->phone}}</td>
						<td>{{$order->order_number}}</td>
						<td>{{$order->is_ready?"已到诊":"未到诊"}}</td>
						<td>{{$order->province_name}}</td>
						<td>{{$order->city_name}}</td>
						<td>{{$order->channel}}</td>
						<td>{{$order->visit_date}}</td>
						<td>{{$order->order_date}}</td>
						<td>{{$order->arrival_date}}</td>
						<td>{{$order->user_name}}</td>
						<td>{{$order->created_at}}</td>
						<td><a href="/order/details?id={{$order->id}}">详情</a></td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>	
	</div>
</div>
@stop
