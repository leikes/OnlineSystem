@extends("layouts.admin.master")
@section("css")
	@parent
@stop

@section('js')
	@parent
	<script type="text/javascript" src="/js/pages/order/list.js"></script>
	<script type="text/javascript" src="/js/pages/page.js"></script>
@stop

		
@section("body")
@include("components.left-nav")
	<div class="master_div">
		<div class="input_box">
			<div class="box_title">
				操作信息
			</div>
			<div class="table_div">
				<table class="order_table">
					<thead>
						<tr>
							<th>编号</th>
							<th>操作</th>
							<th>操作时间</th>
						</tr>
					</thead>
					<tbody id="list_content">
						@foreach($records as $record)
						<tr>
							<td>{{$record['id']}}</td>
							<td>{{$record['record']}}</td>
							<td>{{$record['created_at']}}</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>  
		</div>
		<div class="input_box">
			<div class="box_title">
				分页信息
			</div>
			<div class="pages_div">
				<div class="next_pre_page">
					<a href="{{$records->previousPageUrl()}}">&lt;</a>
				</div>
				<ul id="pagination">
					@for($i=1;$i<=ceil($records->total()/$records->perPage());$i++)
						<li style="display: none;" 
							@if($records->currentPage() == $i)
								class="active"
							@endif
						><a href="{{$records->url($i)}}">{{$i}}</a></li>
					@endfor
				</ul>
				<div class="next_pre_page">
					<a href="{{$records->nextPageUrl()}}">&gt;</a>
				</div>
				<div class="total_page">
					共{{$records->total()}}条记录
				</div>
			</div>
			<div class="page_hide">
				<!-- 数据总数 -->
				<input id="totle_page" type="hidden" name="" value="{{$records->total()}}">
				<!-- 每页显示数据条数 -->
				<input id="per_page" type="hidden" name="" value="{{$records->perPage()}}">
				<!-- 当前页 -->
				<input id="current_page" type="hidden" name="" value="{{$records->currentPage()}}">

				<input id="previous_page_url" type="hidden" name="" value="{{$records->previousPageUrl()}}">

				<input id="next_page_url" type="hidden" name="" value="{{$records->nextPageUrl()}}">
		    </div>
		</div>
	</div>
@stop
