@extends("layouts.admin.master")
@section("css")
		@parent
@stop

@section('js')
		@parent
		<script type="text/javascript" src="/js/pages/order_type/channel.js"></script>
@stop

		
@section("body")
@include("components.left-nav")
<div class="master_div">
	<div class="input_box">
		<div class="box_title">
			咨询渠道列表
		</div>
		<div class="table_div">
			<table class="system_table">
				<thead>
					<tr>
			          <th>编号</th>
			          <th>咨询渠道</th>
			          <th>操作</th>
			        </tr>
				</thead>
				<tbody>
					@foreach($channels as $channel)
					<tr>
						<td class="id">{{$channel['id']}}</td>
						<td class="name"><input type="text" name="" value="{{$channel['channel_name']}}"></td>
						<td><a class="update_a">修改</a>  <a class="delete_a">删除</a></td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
	<div class="input_box">
		<div class="box_title">
			添加咨询渠道
		</div>
		<div class="box_content">
			<div class="input_one">
				<div class="label system_label">咨询渠道：</div>
				<input id="channel_name" type="text" name="">
				<button id="create_btn">确定添加</button>
			</div>
		</div>
	</div>
</div>
@stop
