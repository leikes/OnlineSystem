@extends('layouts.admin.login_master')

@section("css")
    @parent
    <link rel="stylesheet" type="text/css" href="/css/pages/login/login.css">
@stop

@section("body")
    <div class="bg_div">
        <div class="login_div">
            <div class="label_div margin_top1">
                用户名
            </div>
            <div class="input_div">
                <input id="uname" type="text" name="" autocomplete="off">
            </div>
            <div class="label_div margin_top2">
                密码
            </div>
            <div class="input_div">
                <input id="password" type="password" name="" autocomplete="off">
            </div>
            <div class="login_btn">
                <img src="/images/denglu2_03.png">
            </div>
        </div>
    </div>
@stop

@section("js")
    @parent
    <script type="text/javascript" src="/js/pages/login/login.js"></script>
@stop