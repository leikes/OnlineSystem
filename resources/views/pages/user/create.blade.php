@extends("layouts.admin.master")
@section("css")
		@parent
@stop

@section('js')
		@parent
		<script type="text/javascript" src="/js/pages/user/create.js"></script>
@stop

		
@section("body")
@include("components.left-nav")
<div class="master_div">
	<div class="input_box">
		<div class="box_title">
			新建用户
		</div>
		<div class="box_content">
			<div class="input_one">
				<div class="label">姓名：</div>
				<input id="name" type="text" name="">
			</div>
			<div class="input_one">
				<div class="label">用户名：</div>
				<input id="uname" type="text" name="">
			</div>
			<div class="input_one">
				<div class="label">权限组：</div>
				<select id="role_id">
					@foreach($roles as $role)
					<option value="{{$role['id']}}">{{$role['role_name']}}</option>
					@endforeach
				</select>
			</div>
			<div class="input_one">
				<div class="label">密码：</div>
				<input id="password" type="password" name="">
			</div>
		</div>
	</div>
	<div class="input_box">
		<div class="box_title">
			操作
		</div>
		<div class="btn_div">
			<div class="confirm_btn create_btn">
				确定
			</div>
			<div class="cancel_btn">
				取消
			</div>
		</div>
	</div>
</div>
@stop
