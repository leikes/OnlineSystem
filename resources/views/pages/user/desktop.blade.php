@extends("layouts.admin.master")
@section("css")
		@parent
@stop

@section('js')
		@parent
		<script type="text/javascript" src="/js/pages/order/list.js"></script>
		<script type="text/javascript" src="/js/pages/page.js"></script>
@stop

		
@section("body")
@include("components.left-nav")
<div class="master_div">
	<div class="input_box">
		<div class="box_title">
			预约人数
		</div>
		<div class="totle_div">
			<div class="totle_box">
				<div class="left_box">
					本月
				</div>
				<div class="right_box">
					{{$mouthCount}}
				</div>
			</div>
			<div class="totle_box">
				<div class="left_box">
					本周
				</div>
				<div class="right_box">
					{{$weekCount}}
				</div>
			</div>
			<div class="totle_box">
				<div class="left_box">
					本日
				</div>
				<div class="right_box">
					{{$dayCount}}
				</div>
			</div>
		</div>
	</div>
	<div class="input_box">
		<div class="box_title">
			回访人数
		</div>
		<div class="totle_div">
			<div class="totle_box">
				<div class="left_box">
					本月
				</div>
				<div class="right_box">
					{{$visitMouthCount}}
				</div>
			</div>
			<div class="totle_box">
				<div class="left_box">
					本周
				</div>
				<div class="right_box">
					{{$visitWeekCount}}
				</div>
			</div>
			<div class="totle_box">
				<div class="left_box">
					本日
				</div>
				<div class="right_box">
					{{$visitDayCount}}
				</div>
			</div>
		</div>
	</div>
	<div class="input_box">
		<div class="box_title">
			本周预约列表
		</div>
		<div class="table_div">
			<table class="order_table">
				<thead>

					<tr>
						<th>编号</th>
						<th>姓名</th>
						<th>电话</th>
						<th>预约号</th>
						<th>是否到诊</th>
						<th>省份</th>
						<th>城市</th>
						<th>咨询渠道</th>
						<th>回访日期</th>
						<th>预约日期</th>
						<th>到诊日期</th>
						<th>咨询员</th>
						<th>登记时间</th>
						<th>操作</th>
					</tr>
				</thead>
				<tbody>
					@foreach($orderWeekData as $order)
					<tr>
						<td>{{$order->id}}</td>
						<td>{{$order->name}}</td>
						<td>{{$order->phone}}</td>
						<td>{{$order->order_number}}</td>
						<td>{{$order->is_ready?"已到诊":"未到诊"}}</td>
						<td>{{$order->province_name}}</td>
						<td>{{$order->city_name}}</td>
						<td>{{$order->channel}}</td>
						<td>{{$order->visit_date}}</td>
						<td>{{$order->order_date}}</td>
						<td>{{$order->arrival_date}}</td>
						<td>{{$order->user_name}}</td>
						<td>{{$order->created_at}}</td>
						<td><a href="/order/details?id={{$order->id}}">详情</a></td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>  
	</div>
	<div class="input_box">
		<div class="box_title">
			本周回访列表
		</div>
		<div class="table_div">
			<table class="order_table">
				<thead>
					<tr>
						<th>编号</th>
						<th>姓名</th>
						<th>电话</th>
						<th>预约号</th>
						<th>是否到诊</th>
						<th>省份</th>
						<th>城市</th>
						<th>咨询渠道</th>
						<th>回访日期</th>
						<th>预约日期</th>
						<th>到诊日期</th>
						<th>咨询员</th>
						<th>登记时间</th>
						<th>操作</th>
					</tr>
				</thead>
				<tbody>
					@foreach($visitWeekData as $order)
					<tr>
						<td>{{$order->id}}</td>
						<td>{{$order->name}}</td>
						<td>{{$order->phone}}</td>
						<td>{{$order->order_number}}</td>
						<td>{{$order->is_ready?"已到诊":"未到诊"}}</td>
						<td>{{$order->province_name}}</td>
						<td>{{$order->city_name}}</td>
						<td>{{$order->channel}}</td>
						<td>{{$order->visit_date}}</td>
						<td>{{$order->order_date}}</td>
						<td>{{$order->arrival_date}}</td>
						<td>{{$order->user_name}}</td>
						<td>{{$order->created_at}}</td>
						<td><a href="/order/details?id={{$order->id}}">详情</a></td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>  
	</div>
</div>
@stop
