@extends("layouts.admin.master")
@section("css")
    @parent
@stop

@section('js')
    @parent
    <script type="text/javascript" src="/js/pages/user/list.js"></script>
@stop

    
@section("body")
@include("components.left-nav")
<div class="master_div">
  <div class="input_box">
    <div class="box_title">
      用户列表
    </div>
    <div class="table_div">
      <table class="user_table">
        <thead>
          <tr>
            <th>编号</th>
            <th>姓名</th>
            <th>登陆账号</th>
            <th>权限组</th>
            <th>操作</th>
          </tr>
        </thead>
        <tbody>
        @foreach($users as $user)
          <tr>
            <td class='id'>{{$user['id']}}</td>
            <td class="name"><input type="" name="" value="{{$user['name']}}"></td>
            <td class="uname"><input type="" name="" value="{{$user['uname']}}"></td>
            <td class="role">
              <select>
                @foreach($roles as $role)
                <option value="{{$role['id']}}" 
                  @if($user['role']['role_name'] == $role['role_name'])
                    selected
                  @endif
                >{{$role['role_name']}}</option>
                @endforeach
              </select>
            </td>
            <td><a class="update_a">修改</a>&nbsp;&nbsp;<a class="delete_a">删除</a></td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
</div>
@stop
