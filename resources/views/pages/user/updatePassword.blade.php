@extends("layouts.admin.master")
@section("css")
    @parent
@stop

@section('js')
    @parent
    <script type="text/javascript" src="/js/pages/user/update_password.js"></script>
@stop

    
@section("body")
@include("components.left-nav")
<div class="master_div">
  <div class="input_box">
    <div class="box_title">
      修改密码
    </div>
    <div class="box_content">
      <div class="input_one">
        <div class="label">新密码：</div>
        <input id="newPassword" type="password" name="">
      </div>
      <div class="input_one">
        <div class="label">确认密码：</div>
        <input id="confirmPassword" type="password" name="">
      </div>
    </div>
  </div>
  <div class="input_box">
    <div class="box_title">
      操作
    </div>
    <div class="btn_div">
      <div class="confirm_btn">
        确定
      </div>
      <div class="cancel_btn">
        取消
      </div>
    </div>
  </div>
</div>
@stop
