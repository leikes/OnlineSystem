<!DOCTYPE html>
<html lang="zh-CN">
    <head>
        <meta property="qc:admins" content="062327370646252171637570755" />
        <!-- 针对web性能定义字符集，首选通过HTTP header来设置，确保HTTP header和Meta标签设置一致 -->
        <meta charset="UTF-8">
        <meta name="renderer" content="webkit">
        <meta name="viewport" charset="utf-8" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <!-- 告诉IE使用目前最新的布局引擎 -->
        <meta http-equiv="X-UA-Compatible" content="IE=Edge">
        <title>
            @section("title")
                在线预约系统
            @show
        </title>
        <link rel="stylesheet" type="text/css" href="/css/pages/public.css">
        <link rel="stylesheet" type="text/css" href="/css/pages/header.css">
        <link rel="stylesheet" type="text/css" href="/css/pages/style.css">
        @section("css")
        @show
    </head>
    <body ontouchstart>
        <div id="wrapper">
            <div class="body-content">
                @section("body")
                    
                @show
                <div style="clear:both;"></div>
            </div>  
        </div>
        <div class="plugins">
            @section("plugins")
        
            @show
        </div>
        <div class="templates"> 
            @section("templates")
                
            @show
        </div>
        <div class="script">
            @section("js")
                <script type="text/javascript" src="/lib/js/jquery-1.11.2.min.js"></script>
                <script type="text/javascript" src="/js/pages/header.js"></script>
            @show
        </div>
    </body>
</html>