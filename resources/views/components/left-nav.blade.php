@section("css")
    @parent
    <link rel="stylesheet" type="text/css" href="/css/pages/left_nav.css">
@show
    <div class="left_div">
      <ul class="group_ul">
        <li class="group"><img src="/images/yuyue.jpg">预约挂号</li>
        <ul class="chirld_ul">
          <a href="/desktop/index"><li class="chirld"><img src="/images/list_sel.png">我的桌面</li></a>
          <a href="/order/create"><li class="chirld"><img src="/images/list_sel.png">添加/修改记录</li></a>
          <a href="/order/list"><li class="chirld"><img src="/images/list_sel.png">登记管理</li></a>
          <a href="/order/is-ready"><li class="chirld"><img src="/images/list_sel.png">到诊管理</li></a>
          <!-- <a href=""><li class="chirld"><img src="/images/list_sel.png">复诊管理</li></a>
          <a href=""><li class="chirld"><img src="/images/list_sel.png">住院管理</li></a>
          <a href=""><li class="chirld"><img src="/images/list_sel.png">出院管理</li></a> -->
        </ul>
      </ul>
      @if(Auth::user()->role_id == 1 || Auth::user()->role_id == 2)
      <ul class="group_ul">
        <li class="group"><img src="/images/list_user.png">用户管理</li>
        <ul class="chirld_ul">
          <a href="/user/create"><li class="chirld"><img src="/images/list_sel.png">添加用户</li></a>
          <a href="/user/list"><li class="chirld"><img src="/images/list_sel.png">用户列表</li></a>
        </ul>
      </ul>
      <ul class="group_ul">
        <li class="group"><img src="/images/list_set.jpg">系统设置</li>
        <ul class="chirld_ul">
          <a href="/order-type/disease-type"><li class="chirld"><img src="/images/list_sel.png">病种类型设置</li></a>
          <a href="/order-type/register-type"><li class="chirld"><img src="/images/list_sel.png">登记类型设置</li></a>
          <a href="/order-type/channel-type"><li class="chirld"><img src="/images/list_sel.png">咨询渠道设置</li></a>
          <a href="/order-type/doctor-type"><li class="chirld"><img src="/images/list_sel.png">指定专家设置</li></a>
        </ul>
      </ul>
      <ul class="group_ul">
        <li class="group"><img src="/images/list_set.jpg">日志记录</li>
        <ul class="chirld_ul">
          <a href="/record/list"><li class="chirld"><img src="/images/list_sel.png">操作列表</li></a>
        </ul>
      </ul>
      @endif
    </div>
@section("js")
    @parent
    <script type="text/javascript" src="/js/pages/left_nav.js"></script>
@stop