<div class="header_div">
	<div class="logo_div">
		<img src="/images/logo.png">
	</div>
	<div class="menu_div">
		<img src="/images/menu.png">
	</div>
	<div class="user_div">
		<div class="right">
			<div class="user_img"><img src="/images/icon.jpg"></div>
			<div class="user_name">{{Auth::user()->name}}</div>
			<div class="setting_div">
				<img src="/images/setting_icon.png">
			</div>
		</div>
		<div class="set_div">
			<ul>
				<a href="/user/update-password"><li>修改密码</li></a>
				<li id="logout">登出</li>
			</ul>
		</div>
	</div>
</div>
