$ () ->
	$(".menu_div").click (e) ->
		$(".left_div").slideToggle(500)

	$(".user_div").click (e) ->
		$(".set_div").slideToggle(500)

	$("#logout").click (e) ->
		$.post "/api/user/logout",(res) ->
			if res.status.errCode is 0
				window.location.href = "/login"
			else
				alert res.status.message