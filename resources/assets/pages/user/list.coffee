$ () ->
	$(".update_a").click (e) ->
		param=
			id:$(e.target).parent().parent().children(".id").html()
			name:$(e.target).parent().parent().children(".name").children("input").val()
			uname:$(e.target).parent().parent().children(".uname").children("input").val()
			role_id:$(e.target).parent().parent().children(".role").children("select").val()

		$.post "/api/user/update-user", param, (res) ->
			if res.status.errCode is 0
				alert "修改成功"
				window.location.reload()
			else
				alert res.status.message

	$(".delete_a").click (e) ->
		param=
			id:$(e.target).parent().parent().children(".id").html()

		$.post "/api/user/delete", param, (res) ->
			if confirm("确认删除吗？") is false
				return
			else
				if res.status.errCode is 0
					alert "修改成功"
					window.location.reload()
				else
					alert res.status.message
		