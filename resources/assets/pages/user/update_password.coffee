$ () ->
	$(".confirm_btn").click (e) ->
		param=
			newPassword:$("#newPassword").val()
			confirmPassword:$("#confirmPassword").val()
		$.post "/api/user/update-oneself", param, (res) ->
			if res.status.errCode is 0
				alert "修改成功"
				window.location.href = "/desktop/index"
			else
				alert res.status.message

	$(".cancel_btn").click (e) ->
		history.go(-1)
		location.reload()
