$ () ->
	$("#province").change (e) ->
		param =
			'province_id':$("#province").val()

		$.get '/api/address/get-city-province-id',param, (res) ->
			result = [];
			for city in res.data
				result = result + "<option value='"+city.id+"'>"+city.city_name+"</option>"
			$("#city").html(result)

	$("#save_btn").click (e) ->
		if confirm("确认修改吗？") is false
			return
		else
			param =
				id:$("#order_id").val()
				name:$("#name").val()
				phone:$("#phone").val()
				age:$("#age").val()
				sex:$("#sex").val()
				order_date:$("#order_date").val()
				arrival_date:$("#arrival_date").val()
				register_type:$("#register_type").val()
				order_number:$("#order_number").val()
				is_ready:$("#is_ready").val()
				doctor:$("#doctor").val()
				disease_type:$("#disease_type").val()
				channel:$("#channel").val()
				province_id:$("#province").val()
				city_id:$("#city").val()
				address:$("#address").val()
				keyword:$("#keyword").val()
				visit_date:$("#visit_date").val()
				visit_info:$("#visit_info").val()
				user_name:$("#user_name").val()
				remarks:$("#remarks").val()
				record:$("#record").val()
				qq:$("#qq").val()
				wechat:$("#wechat").val()
				clinical:$("#clinical").val()

			$.post '/api/order/update', param, (res) ->
				if res.status.errCode is 0
					alert "修改成功"
					window.location.href = "/order/details?id=" + $("#order_id").val()
				else
					alert res.status.message

	$("#cancel_btn").click (e) ->
		history.go(-1)
		location.reload()
