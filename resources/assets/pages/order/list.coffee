$ () ->
	

	Search = () ->
		if $("#search_input").val() != ""
			return "&" + $("#search_select").val() + "=" + $("#search_input").val()
		else
			return "&" + $("#search_select").val() + "="

	IsReady = () ->
		if $("#is_ready").val() != ""
			return "&is_ready=" + $("#is_ready").val()
		else
			return ""

	VisitDate = () ->
		if $("#visit_date").val() != ""
			return "&visit_date=" + $("#visit_date").val()
		else
			return ""

	Province = () ->
		if $("#province_id").val() != ""
			return "&province_id=" + $("#province_id").val()
		else
			return ""

	City = () ->
		if $("#city_id").val() != ""
			return "&city_id=" + $("#city_id").val()
		else
			return ""

	Address = () ->
		if $("#address").val() != ""
			return "&address=" + $("#address").val()
		else
			return ""



	$("#province_id").change (e) ->
		param =
			'province_id':$("#province_id").val()

		$.get '/api/address/get-city-province-id',param, (res) ->
			result = "<option value=''>--请选择--</option>";
			# console.log res[0]
			for city in res.data
				result = result + "<option value='"+city.id+"'>"+city.city_name+"</option>"
			$("#city_id").empty()
			$("#city_id").append(result)

	$(".sech_btn").click (e) ->
		window.location.href = "/order/list?" + Search() + IsReady() + VisitDate() + Province() + City() + Address()

	$("#is_ready").change (e) ->
		window.location.href = "/order/list?" + Search() + IsReady() + VisitDate() + Province() + City() + Address()

	$("#visit_date").blur (e) ->
		window.location.href = "/order/list?" + Search() + IsReady() + VisitDate() + Province() + City() + Address()

	$("#pagination a").each () ->
		zuiJiaHref = $(this).attr('href')
		$(this).attr('href',zuiJiaHref + Search() + IsReady() + VisitDate() + Province() + City() + Address())

	$("#prePagination a").each () ->
		if $("#previous_page_url").val() != ""
			zuiJiaHref = $(this).attr('href')
			$(this).attr('href',zuiJiaHref + Search() + IsReady() + VisitDate() + Province() + City() + Address())

	$("#nextPagination a").each () ->
		if $("#next_page_url").val() != ""
			zuiJiaHref = $(this).attr('href')
			$(this).attr('href',zuiJiaHref + Search() + IsReady() + VisitDate() + Province() + City() + Address())

	$("#export_btn").click (e) ->
		if $("#list_content").children().length is 0
			alert "无数据导出"
		else
			window.location.href = "/api/order/export?" + Search() + IsReady() + VisitDate() + Province() + City() + Address()
		