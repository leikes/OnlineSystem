$ () ->
	$(".day").click (e) ->
		window.location.href = "/order/is-ready?day=1"

	$(".pre_day").click (e) ->
		window.location.href = "/order/is-ready?preDay=1"

	$(".week").click (e) ->
		window.location.href = "/order/is-ready?week=1"

	$(".pre_week").click (e) ->
		window.location.href = "/order/is-ready?preWeek=1"

	$(".mouth").click (e) ->
		window.location.href = "/order/is-ready?mouth=1"

	$(".pre_mouth").click (e) ->
		window.location.href = "/order/is-ready?preMouth=1"

	$("#start_date").blur (e) ->
		if $("#end_date").val() is ""
			return
		else
			window.location.href = "/order/is-ready?start_date=" + $("#start_date").val() + "&end_date=" + $("#end_date").val()

	$("#end_date").blur (e) ->
		if $("#start_date").val() is ""
			return
		else
			window.location.href = "/order/is-ready?start_date=" + $("#start_date").val() + "&end_date=" + $("#end_date").val()

