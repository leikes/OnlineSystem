$ () ->
	$(".login_bg_div").height($(window).height())

	$(".login_btn").click (e) ->
		param = 
			uname:$("#uname").val()
			password:$("#password").val()

		$.post "/api/user/login", param, (res) -> 
			if res.status.errCode is 0
				window.location.href = "/desktop/index"
			else
				alert res.status.message
