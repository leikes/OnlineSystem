$ () ->
	pageUrl =
			orders: "/order/list"
			orders_create: "/order/create"
			order_type: "/order-type/list"
			users: "/user/list"
			user_create: "/user/create"
			desktop: "/desktop/index"

	for key,value of pageUrl
		if window.location.pathname is value
			$("#"+key).addClass("click-now")

	$(".child").click (e) ->
		for key,value of pageUrl
			if key is this.id
				jumpUrl = key
				break
		return window.location.href =pageUrl[jumpUrl]

	$(".nav_box").height($(document).height())