$ () ->
	$(".update_a").click (e) ->
		param=
			id:$(e.target).parent().parent().children(".id").html()
			disease_name:$(e.target).parent().parent().children(".name").children("input").val()

		$.post '/api/disease/update', param, (res) ->
			if res.status.errCode is 0
				alert "修改成功"
				location.reload()
			else
				alert res.status.message

	$(".delete_a").click (e) ->
		param=
			id:$(e.target).parent().parent().children(".id").html()

		$.post '/api/disease/delete', param, (res) ->
			if res.status.errCode is 0
				alert "删除成功"
				location.reload()
			else
				alert res.status.message

	$("#create_btn").click (e) ->
		param=
			disease_name:$("#disease_name").val()

		$.post '/api/disease/create', param, (res) ->
			if res.status.errCode is 0
				alert "添加成功"
				location.reload()
			else
				alert res.status.message
