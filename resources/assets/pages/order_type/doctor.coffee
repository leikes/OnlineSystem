$ () ->
	$(".update_a").click (e) ->
		param=
			id:$(e.target).parent().parent().children(".id").html()
			doctor_name:$(e.target).parent().parent().children(".name").children("input").val()

		$.post '/api/doctor/update', param, (res) ->
			if res.status.errCode is 0
				alert "修改成功"
				location.reload()
			else
				alert res.status.message

	$(".delete_a").click (e) ->
		param=
			id:$(e.target).parent().parent().children(".id").html()

		$.post '/api/doctor/delete', param, (res) ->
			if res.status.errCode is 0
				alert "删除成功"
				location.reload()
			else
				alert res.status.message

	$("#create_btn").click (e) ->
		param=
			doctor_name:$("#doctor_name").val()

		$.post '/api/doctor/create', param, (res) ->
			if res.status.errCode is 0
				alert "添加成功"
				location.reload()
			else
				alert res.status.message
