$ () ->
	$(".update_a").click (e) ->
		param=
			id:$(e.target).parent().parent().children(".id").html()
			channel_name:$(e.target).parent().parent().children(".name").children("input").val()

		$.post '/api/channel/update', param, (res) ->
			if res.status.errCode is 0
				alert "修改成功"
				location.reload()
			else
				alert res.status.message

	$(".delete_a").click (e) ->
		param=
			id:$(e.target).parent().parent().children(".id").html()

		$.post '/api/channel/delete', param, (res) ->
			if res.status.errCode is 0
				alert "删除成功"
				location.reload()
			else
				alert res.status.message

	$("#create_btn").click (e) ->
		param=
			channel_name:$("#channel_name").val()

		$.post '/api/channel/create', param, (res) ->
			if res.status.errCode is 0
				alert "添加成功"
				location.reload()
			else
				alert res.status.message
