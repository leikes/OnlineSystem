$ () ->
	$(".update_a").click (e) ->
		param=
			id:$(e.target).parent().parent().children(".id").html()
			register_name:$(e.target).parent().parent().children(".name").children("input").val()

		$.post '/api/register/update', param, (res) ->
			if res.status.errCode is 0
				alert "修改成功"
				location.reload()
			else
				alert res.status.message

	$(".delete_a").click (e) ->
		param=
			id:$(e.target).parent().parent().children(".id").html()

		$.post '/api/register/delete', param, (res) ->
			if res.status.errCode is 0
				alert "删除成功"
				location.reload()
			else
				alert res.status.message

	$("#create_btn").click (e) ->
		param=
			register_name:$("#register_name").val()

		$.post '/api/register/create', param, (res) ->
			if res.status.errCode is 0
				alert "添加成功"
				location.reload()
			else
				alert res.status.message
