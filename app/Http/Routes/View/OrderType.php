<?php 
Route::group(['prefix' => 'order-type','namespace' => 'OrderType','middleware'=>'entrance'],function(){

	Route::get('disease-type','OrderTypeController@diseaseType');

	Route::get('register-type','OrderTypeController@registerType');

	Route::get('channel-type','OrderTypeController@channelType');

	Route::get('doctor-type','OrderTypeController@doctorType');
});