<?php 
Route::group(['prefix' => 'user','namespace' => 'User'],function(){
	Route::get('update-password','UserController@updatePassword');
	Route::group(['middleware' => 'role'],function(){
		Route::group(['middleware'=>'entrance'],function(){
			Route::get('list','UserController@index');

			Route::get('create','UserController@create');
		});
	});
});