<?php 
Route::group(['prefix' => 'order','namespace' => 'Order','middleware'=>'entrance'],function(){

	Route::get('list','OrderController@index');

	Route::get('details','OrderController@details');

	Route::get('create','OrderController@create');

	Route::get('update','OrderController@update');

	Route::get("is-ready",'OrderController@isReady');
});