<?php 
Route::group(['prefix'=>'doctor','namespace' => 'Admin'],function(){

	Route::get('get-all','DoctorController@getAll');

	Route::post('create','DoctorController@create');

	Route::post('delete','DoctorController@delete');

	Route::post('update','DoctorController@update');
});