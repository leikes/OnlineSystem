<?php 
Route::group(['prefix'=>'entrance','namespace' => 'Entrance'],function(){

	Route::get('get-all','EntranceController@getAll');

	Route::post('create','EntranceController@create');

	Route::post('delete','EntranceController@delete');

	Route::post('update','EntranceController@update');
});