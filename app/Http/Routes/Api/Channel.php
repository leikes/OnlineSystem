<?php 
Route::group(['prefix'=>'channel','namespace' => 'Admin'],function(){

	Route::get('get-all','ChannelController@getAll');

	Route::post('create','ChannelController@create');

	Route::post('delete','ChannelController@delete');

	Route::post('update','ChannelController@update');
});