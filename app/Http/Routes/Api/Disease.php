<?php 
Route::group(['prefix'=>'disease','namespace' => 'Admin'],function(){

	Route::get('get-all','DiseaseController@getAll');

	Route::post('create','DiseaseController@create');

	Route::post('delete','DiseaseController@delete');

	Route::post('update','DiseaseController@update');
});