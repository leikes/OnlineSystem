<?php 
Route::group(['prefix'=>'register','namespace' => 'Admin'],function(){

	Route::get('get-all','RegisterController@getAll');

	Route::post('create','RegisterController@create');

	Route::post('delete','RegisterController@delete');

	Route::post('update','RegisterController@update');
});