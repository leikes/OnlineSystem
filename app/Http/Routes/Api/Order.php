<?php 
Route::group(['prefix'=>'order','namespace' => 'Order'],function(){

	Route::get('get-all','OrderController@getAll');

	Route::get('get-id','OrderController@getById');

	Route::group(['middleware' => 'orderCreate'],function(){
		Route::post('create','OrderController@create');
	});

	Route::group(['middleware' => 'orderDelete'],function(){
		Route::post('delete','OrderController@delete');
	});

	Route::group(['middleware' => 'orderUpdate'],function(){
		Route::post('update','OrderController@update');
	});

	Route::get('export','OrderController@export');
});