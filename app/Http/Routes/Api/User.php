<?php 
Route::group(['prefix' => 'user' , 'namespace' => 'Customer'],function(){
	

	Route::post('login','UserController@login');
	Route::group(['middleware' => 'auth'],function(){
		Route::get('get-all','UserController@getAll');
		
		Route::group(['middleware' => 'userCreate'],function(){
			Route::post('create','UserController@create');
		});

		Route::group(['middleware' => 'userDelete'],function(){
			Route::post('delete','UserController@delete');
		});
		
		Route::post('update-oneself','UserController@updateOneself');

		Route::post('update-user','UserController@updateUser');

		Route::post('logout','UserController@logout');
	});
	
});
 ?>