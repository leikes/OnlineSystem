<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return redirect()->to('/login');
});

Route::group(['prefix' => 'api','namespace'=>'Api'],function(){
	require_once('Routes/Api/User.php');
	Route::group(['middleware' => 'auth'],function(){
		require_once('Routes/Api/Entrance.php');
		require_once('Routes/Api/Doctor.php');
		require_once('Routes/Api/Register.php');
		require_once('Routes/Api/Disease.php');
		require_once('Routes/Api/Channel.php');
		require_once('Routes/Api/Order.php');
		require_once('Routes/Api/Address.php');
	});
	
});

Route::group(['namespace' => 'View'],function(){
	require_once('Routes/View/Login.php');
	Route::group(['middleware' => 'auth'],function(){
		require_once('Routes/View/Order.php');
		require_once('Routes/View/Desktop.php');
		require_once('Routes/View/User.php');
		Route::group(['middleware' => 'role'],function(){
			require_once('Routes/View/OrderType.php');
			require_once('Routes/View/Record.php');
		});

		
	});
});

Route::get('test','TestController@index');
