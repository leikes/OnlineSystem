<?php

namespace App\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * @var array
     */
    protected $middleware = [
        \Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,
        \App\Http\Middleware\EncryptCookies::class,
        \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
        \Illuminate\Session\Middleware\StartSession::class,
        \Illuminate\View\Middleware\ShareErrorsFromSession::class,
        // \App\Http\Middleware\VerifyCsrfToken::class,
    ];

    /**
     * The application's route middleware.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'auth' => \App\Http\Middleware\Authenticate::class,
        'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'guest' => \App\Http\Middleware\RedirectIfAuthenticated::class,
        'entrance' => \App\Http\Middleware\EntranceSelect::class,
        'role' => \App\Http\Middleware\RoleMiddieware::class,
        'orderCreate' => \App\Http\Middleware\Record\OrderCreate::class,
        'orderUpdate' => \App\Http\Middleware\Record\OrderUpdate::class,
        'orderDelete' => \App\Http\Middleware\Record\OrderDelete::class,
        'userCreate' => \App\Http\Middleware\Record\UserCreate::class,
        'userDelete' => \App\Http\Middleware\Record\UserDelete::class,
    ];
}
