<?php

namespace App\Http\Middleware;

use Closure;
use App;
use Auth;
class EntranceSelect
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::user()->role->role_name != "超级管理员"){
            $request['entrance_id']=Auth::user()->entrance_id;
        }

        return $next($request);
    }
}
