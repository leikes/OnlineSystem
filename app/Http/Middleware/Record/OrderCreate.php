<?php

namespace App\Http\Middleware\Record;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use App\Exceptions\RoleException;
use App\Models\Record;
use Auth;
class OrderCreate
{
    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    /**
     * Create a new middleware instance.
     *
     * @param  Guard  $auth
     * @return void
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $record = Auth::user()->name."新增了一条名字为：‘".$request->name."’的记录，电话为：".$request->phone;
        Record::create([
            'record' => $record
            ]);
        return $next($request);
    }
}
