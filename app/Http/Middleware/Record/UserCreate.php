<?php

namespace App\Http\Middleware\Record;

use Closure;
use App\Exceptions\RoleException;
use App\Models\Record;
use Auth;
use App\Models\User;

class UserCreate
{
    public function handle($request, Closure $next)
    {
        $record = Auth::user()->name."新增了一个名字为：‘".$request->name."’的用户";

        Record::create([
            'record' => $record
            ]);

        return $next($request);
        
    }
}
