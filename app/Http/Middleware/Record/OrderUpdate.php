<?php

namespace App\Http\Middleware\Record;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use App\Exceptions\RoleException;
use App\Models\Record;
use App\Models\Order;
use Auth;
use App\Facades\Utils\ValidateParameter;
class OrderUpdate
{
    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    /**
     * Create a new middleware instance.
     *
     * @param  Guard  $auth
     * @return void
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
         $rules = [
            'id' => 'required|exists:orders,id',
            'name' => 'required',
            'phone' => 'required|unique:orders,phone,'.$request->id.'|numeric',
            'age' => 'numeric',
            'sex' => 'boolean',
            'order_date' => 'date',
            'arrival_date' => 'date',
            'register_type' => 'required|exists:registers,register_name',
            'order_number' => 'required',
            'is_ready' => 'required|boolean',
            'doctor' => 'required|exists:doctors,doctor_name',
            'disease_type' => 'required|exists:diseases,disease_name',
            'province_id' => 'required|exists:provinces,id',
            'city_id' => 'required|exists:citys,id',
            'channel' => 'required|exists:channels,channel_name',
            'visit_date' => 'required|date',
            'user_name' => 'required',
        ];

        ValidateParameter::validate($request,$rules);

        $order = Order::find($request->id);

        $update = "";

        if($order->name != $request->name){
            $update .= "修改了姓名：".$order->name." -> ".$request->name."   ";
        }
        if($order->phone != $request->phone){
            $update .= "修改了电话：".$order->phone." -> ".$request->phone."   ";
        }
        if($order->age != $request->age){
            $update .= "修改了年龄：".$order->age." -> ".$request->age."   ";
        }
        if($order->sex != $request->sex){
            $update .= "修改了性别：".$order->sex." -> ".$request->sex."   ";
        }
        if($order->order_date != $request->order_date){
            $update .= "修改了预约日期：".$order->order_date." -> ".$request->order_date."   ";
        }
        if($order->arrival_date != $request->arrival_date){
            $update .= "修改了到诊日期：".$order->arrival_date." -> ".$request->arrival_date."   ";
        }
        if($order->register_type != $request->register_type){
            $update .= "修改了登记类型：".$order->register_type." -> ".$request->register_type."   ";
        }
        if($order->order_number != $request->order_number){
            $update .= "修改了预约号：".$order->order_number." -> ".$request->order_number."   ";
        }
        if($order->is_ready != $request->is_ready){
            $update .= "修改了是否到诊：".$order->is_ready." -> ".$request->is_ready."   ";
        }
        if($order->doctor != $request->doctor){
            $update .= "修改了指定专家：".$order->doctor." -> ".$request->doctor."   ";
        }
        if($order->user_name != $request->user_name){
            $update .= "修改了咨询员：".$order->user_name." -> ".$request->user_name."   ";
        }
        if($order->disease_type != $request->disease_type){
            $update .= "修改了疾病类型：".$order->disease_type." -> ".$request->disease_type."   ";
        }
        if($order->province_id != $request->province_id){
            $update .= "修改了省份：".$order->province_id." -> ".$request->province_id."   ";
        }
        if($order->city_id != $request->city_id){
            $update .= "修改了城市：".$order->city_id." -> ".$request->city_id."   ";
        }
        if($order->channel != $request->channel){
            $update .= "修改了咨询渠道：".$order->channel." -> ".$request->channel."   ";
        }
        if($order->keyword != $request->keyword){
            $update .= "修改了搜索关键词：".$order->keyword." -> ".$request->keyword."   ";
        }
        if($order->remarks != $request->remarks){
            $update .= "修改了备注：".$order->remarks." -> ".$request->remarks."   ";
        }
        if($order->record != $request->record){
            $update .= "修改了聊天记录：".$order->record." -> ".$request->record."   ";
        }
        if($order->visit_date != $request->visit_date){
            $update .= "修改了回访日期：".$order->visit_date." -> ".$request->visit_date."   ";
        }
        if($order->visit_info != $request->visit_info){
            $update .= "修改了回访信息：".$order->visit_info." -> ".$request->visit_info."   ";
        }

        $record = Auth::user()->name."修改了一条记录!".$update;
        Record::create([
            'record' => $record
            ]);
        return $next($request);
        
    }
}
