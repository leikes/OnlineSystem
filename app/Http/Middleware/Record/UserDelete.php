<?php

namespace App\Http\Middleware\Record;

use Closure;
use App\Exceptions\RoleException;
use App\Models\Record;
use Auth;
use App\Models\User;
class UserDelete
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user=User::find($request->id);

        $record = Auth::user()->name."删除了一个名字为：‘".$user->name."’的用户";

        Record::create([
            'record' => $record
            ]);

        return $next($request);
        
    }
}
