<?php

namespace App\Http\Middleware\Record;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use App\Exceptions\RoleException;
use App\Models\Record;
use Auth;
use App\Models\Order;
class OrderDelete
{
    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    /**
     * Create a new middleware instance.
     *
     * @param  Guard  $auth
     * @return void
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $order=Order::find($request->id);

        $record = Auth::user()->name."删除了一条名字为：‘".$order->name."’的记录";
        Record::create([
            'record' => $record
            ]);
        return $next($request);
        
    }
}
