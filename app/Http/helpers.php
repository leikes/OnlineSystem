<?php

if(!function_exists('formatResponse')){

    /**
     * 格式化响应数据
     *
     * @param   string|int      $errCode
     * @param   string          $message
     * @param   mixed           $data
     * @return  array
     */
    function formatResponse($errCode = 0, $message = null, $data = null, $locale = null){

        $response = [
            'status' => [
                'errCode' => $errCode,
                'message' => $message,
                'locale' => $locale
            ],
        ];
        

        if(!is_null($data)){
            array_set($response, 'data', $data); 
        }

        return $response;
    }
}

if(!function_exists('formatJsonResponse')){

    /**
     * 格式化响应数据
     *
     * @param   mixed           $data
     * @param   string          $message
     * @param   string|int      $errCode
	 * @return  \Illuminate\Http\JsonResponse
     */
    function formatJsonResponse($data = null, $message = '正常', $errCode = 0){

        $locale=App::getLocale();

        if($locale=='zh-cn'){
            $locale='cn';
        }

        return Response::json(formatResponse($errCode, $message, $data, $locale));
    }
}

if(!function_exists('isDebugging')){

    /**
     * 检测是否调试模式
     *
     * @return bool
     */
    function isDebugging(){
        
        return Config::get('app.debug'); 
    }
}

if (!function_exists('filterToArray')) {
    function filterToArray($data,$key,$value)
    {       
        $array = [];
        foreach($data as $object)
        {   
            $array = array_add($array, $object->$key, $object->$value);
        }   
        return $array;
    }
}

