<?php

namespace App\Http\Controllers\View\User;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Facades\Product\DesktopService;
class DesktopController extends Controller
{
    public function index()
    {
        // dd(DesktopService::getWeekVisitData());
        return view('pages.user.desktop',[
            'mouthCount' => DesktopService::getMouthCount(),
            'weekCount' => DesktopService::getWeekCount(),
            'dayCount' => DesktopService::getDayCount(),
            'visitMouthCount' => DesktopService::getMouthVisitCount(),
            'visitWeekCount' => DesktopService::getWeekVisitCount(),
            'visitDayCount' => DesktopService::getDayVisitCount(),
            'visitWeekData' => DesktopService::getWeekVisitData(),
            'orderWeekData' => DesktopService::getWeekOrderData()
            ]);
    }
}
