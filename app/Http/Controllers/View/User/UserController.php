<?php

namespace App\Http\Controllers\View\User;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Facades\Product\UserService;
use App\Facades\Product\RoleService;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return view('pages.user.list',[
        	'users' => UserService::getAllByConditions($request),
        	'roles' => RoleService::getAll()
        	]);
    }

    public function create(Request $request)
    {
        return view('pages.user.create',['roles' => RoleService::getAll()]);
    }

    public function updatePassword(Request $request)
    {
        return view('pages.user.updatePassword');
    }
}
