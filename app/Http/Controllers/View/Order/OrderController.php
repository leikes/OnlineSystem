<?php

namespace App\Http\Controllers\View\Order;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Facades\Product\OrderService;
use App\Facades\Product\DoctorService;
use App\Facades\Product\ChannelService;
use App\Facades\Product\DiseaseService;
use App\Facades\Product\RegisterService;
use App\Facades\Product\AddressService;
use App\Facades\Product\UserService;
use App\Facades\Product\TotleService;
use App\Facades\Utils\OrderNumber;

class OrderController extends Controller
{
    public function index(Request $request)
    {
        $conditions = $request->all();

        $citys = [];

        if(isset($conditions['province_id']) && $conditions['province_id'] != ''){
            $citys = AddressService::getAllCityByProvinceId($request);
        }

        return view('pages.order.list',[
            'orders' => OrderService::getAll($request),
            'conditions' => $conditions,
            'provinces' => AddressService::getAllProvince(),
            'citys' => $citys
            ]);
    }

    public function details(Request $request)
    {
        $order = OrderService::getById($request);

    	return view('pages.order.details',[
    		'order' => $order,
    		]);
    }

    public function create(Request $request){
        return view('pages.order.create',[
            'diseases' => DiseaseService::getAll($request),
            'registers' => RegisterService::getAll($request),
            'channels' => ChannelService::getAll($request),
            'doctors' => DoctorService::getAll($request),
            'provinces' => AddressService::getAllProvince(),
            'orderNumber' => OrderNumber::getOrderNumber()
            ]);
    }

    public function update(Request $request){
        $order = OrderService::getById($request);
        $request->province_id = $order['province_id'];
        return view('pages.order.update',[
            'order' => $order,
            'diseases' => DiseaseService::getAll($request),
            'registers' => RegisterService::getAll($request),
            'channels' => ChannelService::getAll($request),
            'doctors' => DoctorService::getAll($request),
            'provinces' => AddressService::getAllProvince(),
            'citys' => AddressService::getAllCityByProvinceId($request),
            'users' => UserService::getAllByConditions($request)
            ]);
    }

    public function isReady(Request $request)
    {   
        // dd(TotleService::getIsReadyTotle($request));
        return view('pages.order.is_ready',[
            'orders' => TotleService::getIsReadyTotle($request),
            'param' => TotleService::dateParam($request)
            ]);
    }
}
