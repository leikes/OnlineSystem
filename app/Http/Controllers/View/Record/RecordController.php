<?php

namespace App\Http\Controllers\View\Record;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Record;
class RecordController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $records = Record::orderBy('created_at','desc')->paginate(15);
        return view('pages.log.list',['records' => $records]);
    }
}
