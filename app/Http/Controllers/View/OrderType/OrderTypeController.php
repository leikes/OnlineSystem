<?php

namespace App\Http\Controllers\View\OrderType;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Facades\Product\DiseaseService;
use App\Facades\Product\RegisterService;
use App\Facades\Product\ChannelService;
use App\Facades\Product\DoctorService;
class OrderTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function diseaseType(Request $request)
    {
        return view('pages.order_type.disease_type',[
            'diseases' => DiseaseService::getAll($request)
            ]);
    }

    public function registerType(Request $request)
    {
        return view('pages.order_type.register_type',[
            'registers' =>RegisterService::getAll($request)
            ]);
    }

    public function channelType(Request $request)
    {
        return view('pages.order_type.channel_type',[
            'channels' => ChannelService::getAll($request)
            ]);
    }

    public function doctorType(Request $request)
    {
        return view('pages.order_type.doctor_type',[
            'doctors' => DoctorService::getAll($request)
            ]);
    }
}
