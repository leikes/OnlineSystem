<?php

namespace App\Http\Controllers\Api\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Facades\Product\DiseaseService;
use App\Facades\Utils\ValidateParameter;

class DiseaseController extends Controller
{
    public function getAll(Request $request){

        return formatJsonResponse(DiseaseService::getAll($request));
    }

    public function create(Request $request){
        $rules = [
            'disease_name' => 'required'
        ];

        ValidateParameter::validate($request,$rules);

        DiseaseService::createDisease($request);

        return formatJsonResponse();
    }

    public function update(Request $request){
        $rules = [
            'id' => 'required|exists:diseases,id',
            'disease_name' => 'required'
        ];

        ValidateParameter::validate($request,$rules);

        DiseaseService::updateDisease($request);

        return formatJsonResponse();
    }

    public function delete(Request $request){
        $rules = [
            'id' => 'required|exists:diseases,id'
        ];

        ValidateParameter::validate($request,$rules);

        DiseaseService::deleteDisease($request);

        return formatJsonResponse();
    }
}
