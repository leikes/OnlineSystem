<?php

namespace App\Http\Controllers\Api\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Facades\Product\ChannelService;
use App\Facades\Utils\ValidateParameter;

class ChannelController extends Controller
{
     public function getAll(Request $request){

        return formatJsonResponse(ChannelService::getAll($request));
    }

    public function create(Request $request){
        $rules = [
            'channel_name' => 'required'
        ];

        ValidateParameter::validate($request,$rules);

        ChannelService::createChannel($request);

        return formatJsonResponse();
    }

    public function update(Request $request){
        $rules = [
            'id' => 'required|exists:channels,id',
            'channel_name' => 'required'
        ];

        ValidateParameter::validate($request,$rules);

        ChannelService::updateChannel($request);

        return formatJsonResponse();
    }

    public function delete(Request $request){
        $rules = [
            'id' => 'required|exists:channels,id'
        ];

        ValidateParameter::validate($request,$rules);

        ChannelService::deleteChannel($request);

        return formatJsonResponse();
    }
}
