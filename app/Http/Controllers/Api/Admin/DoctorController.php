<?php

namespace App\Http\Controllers\Api\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Facades\Product\DoctorService;
use App\Facades\Utils\ValidateParameter;

class DoctorController extends Controller
{
    public function getAll(Request $request){

        return formatJsonResponse(DoctorService::getAll($request));
    }

    public function create(Request $request){
    	$rules = [
    		'doctor_name' => 'required'
    	];

    	ValidateParameter::validate($request,$rules);

    	DoctorService::createDoctor($request);

    	return formatJsonResponse();
    }

    public function update(Request $request){
    	$rules = [
    		'id' => 'required|exists:doctors,id',
    		'doctor_name' => 'required'
    	];

    	ValidateParameter::validate($request,$rules);

    	DoctorService::updateDoctor($request);

    	return formatJsonResponse();
    }

    public function delete(Request $request){
    	$rules = [
    		'id' => 'required|exists:doctors,id'
    	];

    	ValidateParameter::validate($request,$rules);

    	DoctorService::deleteDoctor($request);

    	return formatJsonResponse();
    }
}
