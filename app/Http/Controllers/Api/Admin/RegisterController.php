<?php

namespace App\Http\Controllers\Api\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Facades\Product\RegisterService;
use App\Facades\Utils\ValidateParameter;

class RegisterController extends Controller
{
    public function getAll(Request $request){

        return formatJsonResponse(RegisterService::getAll($request));
    }

    public function create(Request $request){
        $rules = [
            'register_name' => 'required'
        ];

        ValidateParameter::validate($request,$rules);

        RegisterService::createRegister($request);

        return formatJsonResponse();
    }

    public function update(Request $request){
        $rules = [
            'id' => 'required|exists:registers,id',
            'register_name' => 'required'
        ];

        ValidateParameter::validate($request,$rules);

        RegisterService::updateRegister($request);

        return formatJsonResponse();
    }

    public function delete(Request $request){
        $rules = [
            'id' => 'required|exists:registers,id'
        ];

        ValidateParameter::validate($request,$rules);

        RegisterService::deleteRegister($request);

        return formatJsonResponse();
    }
}
