<?php

namespace App\Http\Controllers\Api\Order;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Facades\Product\OrderService;
use App\Facades\Utils\ValidateParameter;

class OrderController extends Controller
{
    public function getAll(Request $request)
    {
        return formatJsonResponse(OrderService::getAll($request));
    }

    public function export(Request $request){

        OrderService::exportExcel($request);
        
        return formatJsonResponse();
    }

    public function getById(Request $request)
    {
        return formatJsonResponse(OrderService::getById($request));
    }

    public function update(Request $request)
    {
        $rules = [
            'id' => 'required|exists:orders,id',
            'name' => 'required',
            'phone' => 'required|unique:orders,phone,'.$request->id.'|numeric',
            'age' => 'numeric',
            'sex' => 'boolean',
            'order_date' => 'date',
            'arrival_date' => 'date',
            'register_type' => 'required|exists:registers,register_name',
            'order_number' => 'required',
            'is_ready' => 'required|boolean',
            'doctor' => 'required|exists:doctors,doctor_name',
            'disease_type' => 'required|exists:diseases,disease_name',
            'province_id' => 'required|exists:provinces,id',
            'city_id' => 'required|exists:citys,id',
            'channel' => 'required|exists:channels,channel_name',
            'visit_date' => 'required|date',
            'user_name' => 'required',
            'qq' => 'numeric',
        ];

        ValidateParameter::validate($request,$rules);

        OrderService::updateOrder($request);

        return formatJsonResponse();
    }

    public function delete(Request $request)
    {
        $rules = [
            'id' => 'required|exists:orders,id'
        ];

        ValidateParameter::validate($request,$rules);

        OrderService::deleteOrder($request);

        return formatJsonResponse();
    }

    public function create(Request $request)
    {
        $rules = [
            'name' => 'required',
            'phone' => 'required|unique:orders,phone|numeric',
            'age' => 'numeric',
            'sex' => 'boolean',
            'order_date' => 'date',
            'arrival_date' => 'date',
            'register_type' => 'required|exists:registers,register_name',
            'order_number' => 'required',
            'is_ready' => 'required|boolean',
            'doctor' => 'required|exists:doctors,doctor_name',
            'disease_type' => 'required|exists:diseases,disease_name',
            'province_id' => 'required|exists:provinces,id',
            'city_id' => 'required|exists:citys,id',
            'channel' => 'required|exists:channels,channel_name',
            'visit_date' => 'required|date',
            'qq' => 'numeric',
        ];

        ValidateParameter::validate($request,$rules);

        OrderService::createOrder($request);

        return formatJsonResponse();

    }
}
