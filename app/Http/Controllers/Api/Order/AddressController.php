<?php

namespace App\Http\Controllers\Api\Order;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Facades\Product\AddressService;
use App\Facades\Utils\ValidateParameter;

class AddressController extends Controller
{
    public function getAllProvince(){
        return formatJsonResponse(AddressService::getAllProvince());
    }

    public function getAllCityByProvinceId(Request $request){
        $rules = [
            'province_id' => 'required|exists:provinces,id'
        ];

        ValidateParameter::validate($request,$rules);
        
        return formatJsonResponse(AddressService::getAllCityByProvinceId($request));
    }
}
