<?php

namespace App\Http\Controllers\Api\Entrance;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Facades\Product\EntranceService;
use App\Facades\Utils\ValidateParameter;

class EntranceController extends Controller
{
   public function getAll(){
        return formatJsonResponse(EntranceService::getAll());
   }

   public function create(Request $request){

        $rules=[
            'entrance_name' => 'required|unique:entrances,entrance_name'
        ];

        ValidateParameter::validate($request,$rules);

        EntranceService::createEntrance($request);

        return formatJsonResponse();
   }

   public function delete(Request $request){

        $rules=[
            'id' => 'required|exists:entrances,id'
        ];

         ValidateParameter::validate($request,$rules);

         EntranceService::deleteEntrance($request);

         return formatJsonResponse();
   }

   public function update(Request $request){
        $rules=[
            'id' => 'required|exists:entrances,id',
            'entrance_name' => 'required|unique:entrances,entrance_name,'.$request->id
        ];

        ValidateParameter::validate($request,$rules);

        EntranceService::updateEntrance($request);

        return formatJsonResponse();
   }
}
