<?php namespace App\Services\Product;

use App\Facades\Infra\AdminManager;
use Illuminate\Http\Request;
use App\Facades\Utils\ValidateParameter;
use Auth;
class RegisterService{

	public function getAll(Request $request){

		$conditions = $request->all();

		unset($conditions['id']);
		
		return AdminManager::getRegisterByConditions($conditions);
	}

	public function createRegister(Request $request){

		$data = $request->all();

		if(Auth::user()->entrance_id==1){
			$rules = [
				'entrance_id' => 'required|exists:entrances,id',
			];

			ValidateParameter::validate($request,$rules);
		}

		else{
			$data['entrance_id'] = Auth::user()->entrance_id;
		}

		AdminManager::createRegister($data);
	}

	public function updateRegister(Request $request){

		$data = $request->all();

		$id = $data['id'];

		AdminManager::updateRegister($id,$data);
	}

	public function deleteRegister(Request $request){
		$id = $request->id;

		AdminManager::deleteRegister($id);
	}
}