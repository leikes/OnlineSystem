<?php 
namespace App\Services\Product;
use DB;
use App\Models\Order;
use Auth;
class TotleService{
	public function getIsReadyTotle($request){
		// dd($this->sql().$this->preDay().$this->addWhere());
		$data=$request->all();

		$totleSql = $this->sql();

		if(isset($data['day']) 
			&& isset($data['preDay']) 
			&& isset($data['week']) 
			&& isset($data['preWeek']) 
			&& isset($data['mouth']) 
			&& isset($data['preMouth']) 
			&& isset($data['start_date']) 
			&& isset($data['end_date']))
		{
			$totleSql= $totleSql.$this->day();
		}

		if(isset($data['day']) && $data['day'] == 1){
			$totleSql= $totleSql.$this->day();
		}

		if(isset($data['preDay']) && $data['preDay'] == 1){
			$totleSql= $totleSql.$this->preDay();
		}

		if(isset($data['week']) && $data['week'] == 1){
			$totleSql= $totleSql.$this->week();
		}

		if(isset($data['preWeek']) && $data['preWeek'] == 1){
			$totleSql= $totleSql.$this->preWeek();
		}

		if(isset($data['mouth']) && $data['mouth'] == 1){
			$totleSql= $totleSql.$this->mouth();
		}

		if(isset($data['preMouth']) && $data['preMouth'] == 1){
			$totleSql= $totleSql.$this->preMouth();
		}

		if(isset($data['start_date']) && isset($data['end_date'])){
			return $this->date($data['start_date'],$data['end_date']);
		}
		else{
			$totleSql = $totleSql.$this->addWhere();
			return DB::select($totleSql);
		}
	}

	protected function sql(){
		return "SELECT orders.*,provinces.province_name,citys.city_name FROM orders  LEFT JOIN provinces ON orders.province_id = provinces.id LEFT JOIN citys ON orders.city_id = citys.id WHERE";
	}

	protected function addWhere(){
		$sql = " is_ready = 1";
		if(Auth::user()->role_id != 1){
			$sql =$sql." and orders.entrance_id=".Auth::user()->entrance_id;
		}
		$sql=$sql." ORDER BY created_at DESC";

		return $sql;
	}

	protected function day(){
		return " date_format(arrival_date,'%Y-%m-%d') = date_format(now(),'%Y-%m-%d') and arrival_date != null and";
	}

	protected function preDay(){
		return " date_format(date_add(arrival_date,interval 1 day),'%Y-%m-%d') = date_format(now(),'%Y-%m-%d') and arrival_date != null and";
	}

	protected function week(){
		return " YEARWEEK(date_format(orders.arrival_date,'%Y-%m-%d')) = YEARWEEK(now()) and arrival_date != null and";
	}

	protected function preWeek(){
		return " YEARWEEK(date_format(date_add(orders.arrival_date,interval 1 week),'%Y-%m-%d')) = YEARWEEK(now()) and arrival_date != null and";
	}

	protected function mouth(){
		return " date_format(orders.arrival_date,'%Y-%m') = date_format(now(),'%Y-%m') and orders.arrival_date != null and";
	}

	protected function preMouth(){
		return " date_format(date_add(orders.arrival_date, interval 1 month),'%Y-%m') = date_format(now(),'%Y-%m') and orders.arrival_date != null and";
	}

	protected function date($startDate,$endDate){
		return Order::where('arrival_date','>=',$startDate)
					  ->where('arrival_date','<',$endDate)
					  ->where('is_ready','1')
					  ->get();
	}

	public function dateParam($request){
		$data = $request->all();

		$param=[
			'startDate' => "",
			'endDate' => ""
		];

		if(isset($data['start_date']) && isset($data['end_date'])){
			$param=[
				'startDate' => $data['start_date'],
				'endDate' => $data['end_date']
			];
		}

		return $param;
	}
}