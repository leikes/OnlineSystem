<?php namespace App\Services\Product;

use App\Facades\Infra\AdminManager;
use Illuminate\Http\Request;
use App\Facades\Infra\AddressManager;

class AddressService{
	public function getAllProvince(){
		return AddressManager::getAllProvince();
	}

	public function getAllCityByProvinceId(Request $request)
	{
		$provinceId = $request->province_id;

		$conditions = [
			'province_id' => $provinceId
		];

		return AddressManager::getAllCityByProvinceId($conditions);
	}
}