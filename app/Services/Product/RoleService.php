<?php
namespace App\Services\Product;

use App\Facades\Infra\UserManager;
use Auth;
class RoleService{
	
	public function getAll(){
		$conditions =[];

		if(Auth::user()->role->id != 1){
			$conditions['id'] = 1;
		}
		return UserManager::getAllRoles($conditions);
	}
}