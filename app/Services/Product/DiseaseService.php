<?php namespace App\Services\Product;

use App\Facades\Infra\AdminManager;
use Illuminate\Http\Request;
use App\Facades\Utils\ValidateParameter;
use Auth;
class DiseaseService{

	public function getAll(Request $request){

		$conditions = $request->all();

		unset($conditions['id']);

		return AdminManager::getDiseaseByConditions($conditions);
	}

	public function createDisease(Request $request){

		$data = $request->all();

		if(Auth::user()->entrance_id==1){
			$rules = [
				'entrance_id' => 'required|exists:entrances,id',
			];

			ValidateParameter::validate($request,$rules);
		}

		else{
			$data['entrance_id'] = Auth::user()->entrance_id;
		}

		AdminManager::createDisease($data);
	}

	public function updateDisease(Request $request){

		$data = $request->all();

		$id = $data['id'];

		AdminManager::updateDisease($id,$data);
	}

	public function deleteDisease(Request $request){
		$id = $request->id;

		AdminManager::deleteDisease($id);
	}
}