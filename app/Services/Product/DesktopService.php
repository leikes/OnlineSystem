<?php namespace App\Services\Product;

use App\Facades\Infra\DesktopManager;
use DB;
use Auth;
class DesktopService{

	public function getMouthCount(){
		$sql = "SELECT * FROM orders WHERE date_format(created_at,'%Y-%m') = date_format(now(),'%Y-%m')";

		return count(DB::select($this->sql($sql)));
	}

	public function getWeekCount(){
		$sql = "SELECT * FROM orders WHERE YEARWEEK(date_format(created_at,'%Y-%m-%d')) = YEARWEEK(now())";

		return count(DB::select($this->sql($sql)));
	}

	public function getDayCount(){
		$sql = "SELECT * FROM orders WHERE date_format(created_at,'%Y-%m-%d') = date_format(now(),'%Y-%m-%d')";
		
		return count(DB::select($this->sql($sql)));
	}

	public function getMouthVisitCount()
	{
		$sql = "SELECT * FROM orders WHERE date_format(visit_date,'%Y-%m') = date_format(now(),'%Y-%m')";

		return count(DB::select($this->sql($sql)));
	}

	public function getWeekVisitCount(){
		$sql = "SELECT * FROM orders WHERE YEARWEEK(date_format(visit_date,'%Y-%m-%d')) = YEARWEEK(now())";

		return count(DB::select($this->sql($sql)));
	}

	public function getDayVisitCount(){
		$sql = "SELECT * FROM orders WHERE date_format(visit_date,'%Y-%m-%d') = date_format(now(),'%Y-%m-%d')";
		
		return count(DB::select($this->sql($sql)));
	}

	public function getWeekVisitData(){
		$sql = "SELECT o.*,p.province_name,c.city_name FROM orders o LEFT JOIN provinces p ON o.province_id = p.id LEFT JOIN citys c ON o.city_id = c.id WHERE YEARWEEK(date_format(o.visit_date,'%Y-%m-%d')) = YEARWEEK(now())";

		return DB::select($this->sql($sql));
	}

	public function getDayVisitData(){
		$sql = "SELECT o.*,p.province_name,c.city_name FROM orders o LEFT JOIN provinces p ON o.province_id = p.id LEFT JOIN citys c ON o.city_id = c.id WHERE date_format(visit_date,'%Y-%m-%d') = date_format(now(),'%Y-%m-%d')";

		return DB::select($this->sql($sql));
	}

	public function getWeekOrderData(){
		$sql = "SELECT o.*,p.province_name,c.city_name FROM orders o LEFT JOIN provinces p ON o.province_id = p.id LEFT JOIN citys c ON o.city_id = c.id WHERE YEARWEEK(date_format(o.created_at,'%Y-%m-%d')) = YEARWEEK(now())";

		return DB::select($this->sql($sql));
	}

	protected function sql($sql){
		if(Auth::user()->role_id != 1){
			$sql = $sql." AND entrance_id =".Auth::user()->entrance_id." ORDER BY created_at DESC";
		}
		return $sql;
	}
}