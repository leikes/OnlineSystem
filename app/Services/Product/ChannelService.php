<?php namespace App\Services\Product;

use App\Facades\Infra\AdminManager;
use Illuminate\Http\Request;
use App\Facades\Utils\ValidateParameter;
use Auth;
class ChannelService{

	public function getAll(Request $request){

		$conditions = $request->all();

		unset($conditions['id']);
		
		return AdminManager::getChannelByConditions($conditions);
	}

	public function createChannel(Request $request){

		$data = $request->all();

		if(Auth::user()->entrance_id==1){
			$rules = [
				'entrance_id' => 'required|exists:entrances,id',
			];

			ValidateParameter::validate($request,$rules);
		}

		else{
			$data['entrance_id'] = Auth::user()->entrance_id;
		}

		AdminManager::createChannel($data);
	}

	public function updateChannel(Request $request){

		$data = $request->all();

		$id = $data['id'];

		AdminManager::updateChannel($id,$data);
	}

	public function deleteChannel(Request $request){
		$id = $request->id;

		AdminManager::deleteChannel($id);
	}
}