<?php namespace App\Services\Product;

use App\Facades\Infra\EntranceManager;
use Illuminate\Http\Request;

class EntranceService{

	public function getAll(){
		return EntranceManager::getAllByConditions();
	}

	public function createEntrance(Request $request){

		$data=$request->all();

		EntranceManager::createEntrance($data);
	}

	public function deleteEntrance(Request $request){
		$id=$request->id;

		EntranceManager::deleteEntrance($id);
	}

	public function updateEntrance(Request $request){
		$data=$request->all();

		$id=$data['id'];

		EntranceManager::updateEntrance($id,$data);
	}
}