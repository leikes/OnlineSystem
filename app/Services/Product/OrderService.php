<?php namespace App\Services\Product;

use Illuminate\Http\Request;
use App\Facades\Infra\OrderManager;
use Auth;
use App\Facades\Utils\ExportExcel;
use App\Facades\Utils\ValidateParameter;

class OrderService{

	public function getAll(Request $request)
	{
		$conditions = $request->all();

		// dd($this->groupConditionsHasProvince($conditions));

		$orders = OrderManager::getAllByConditions($this->groupConditionsNotProvince($conditions));

		$orders = OrderManager::getAllByConditionsNotLike($this->groupConditionsHasProvince($conditions), $orders);

		// dd($orders->get()->toArray());

		return $orders->orderBy('created_at','DESC')->paginate(15);
	}

	public function groupConditionsNotProvince($conditions){
		$conditions  = array_except($conditions,['province_id']);

		$conditions  = array_except($conditions,['city_id']);

		return $conditions;
	}

	public function groupConditionsHasProvince($conditions){
		$conditions  = array_only($conditions,['province_id', 'city_id']);

		return $conditions;
	}

	public function getById(Request $request)
	{
		$id = $request->id;

		return OrderManager::getById($id);
	}

	public function createOrder(Request $request)
	{
		$data = $request->all();

		if($data['is_ready'] == 1){
			$rules = [
            'arrival_date' => 'required|date'
	        ];

	        ValidateParameter::validate($request,$rules);
		}
		
		
		$data['entrance_id'] = Auth::user()->entrance_id;

		$data['user_name'] = Auth::user()->name;

		OrderManager::createOrder($data);
	}

	public function deleteOrder(Request $request)
	{
		$id = $request->id;

		OrderManager::deleteOrder($id);
	}

	public function updateOrder(Request $request){
		$data = $request->all();

		if($data['is_ready'] == 1){
			$rules = [
            'arrival_date' => 'required|date'
	        ];

	        ValidateParameter::validate($request,$rules);
		}
		
		$id = $request->id;

		unset($data['id']);

		OrderManager::updateOrder($id,$data);
	}

	public function exportExcel(Request $request)
	{
		$conditions = $request->all();

		$orders = OrderManager::getAllByConditions($this->groupConditionsNotProvince($conditions));

		$orders = OrderManager::getAllByConditionsNotLike($this->groupConditionsHasProvince($conditions), $orders);

		ExportExcel::export('预约信息',$this->formatExport($orders->orderBy('created_at','desc')->get()->toArray()));
	}

	private function formatExport($datas){

		$keyMap=[
			'id' => '编号',
            'name' => '姓名',
	    	'phone' => '电话',
	    	'age' => '年龄',
	    	'sex_desc' => '性别',
	    	'order_date' => '预约日期',
	    	'arrival_date' => '到诊日期',
	    	'register_type' => '登记类型',
	    	'order_number' => '预约号',
	    	'ready_desc' => '是否到诊',
	    	'doctor' => '指定专家',
	    	'user_name' => '咨询员',
	    	'disease_type' => '疾病类型',
	    	'province.province_name' => '所在省份',
	    	'city.city_name' => '所在城市',
	    	'address' => '详细地址',
	    	'channel' => '咨询渠道',
	    	'keyword' => '搜索关键词',
	    	'remarks' => '备注',
	    	'record' => '聊天记录',
	    	'visit_date' => '回访日期',
	    	'visit_info' => '个案后续跟踪记录',
	    	'qq' => 'qq',
	    	'wechat' => '微信',
	    	'created_at' => '登记时间',
	    	'clinical' => '个案回访记录'
        ];

        $Export=[];

        foreach($datas  as  $data)
        {
            $_data=[];
            foreach($keyMap as $key=>$value)
            {
                $_data[$value]= array_get($data,$key,'');
            }

            array_push($Export, $_data);
        }

        // dd($Export);

        return $Export;
	}
}