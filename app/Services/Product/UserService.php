<?php
namespace App\Services\Product;

use App\Facades\Infra\UserManager;
use Auth;
use App\Facades\Utils\ValidateParameter;
use Illuminate\Http\Request;

class UserService{
	
	/**
     * 获取全部用户
     * @return [type] [description]
     */
	public function getAllByConditions(Request $request){

		$conditions = $request->all();

		unset($conditions['id']);
		
		return UserManager::getAllByConditions($conditions);
	}

	/**
     * 添加用户
     * @param  Request $request [description]
     * @return [type]           [description]
     */
	public function createUser(Request $request){

		$data = $request->all();

		if(Auth::user()->entrance_id==1){
			$rules = [
				'entrance_id' => 'required|exists:entrances,id',
			];

			ValidateParameter::validate($request,$rules);
		}

		else{
			$data['entrance_id'] = Auth::user()->entrance_id;
		}

		$data['password'] = bcrypt($data['password']);

		UserManager::createUser($data);
	}

	/**
    * 根据用户id删除用户
    * @param  Request $request [description]
    * @return [type]           [description]
    */
	public function deleteUser(Request $request){

		$data = $request->all();

		$id = $data['id'];

		UserManager::deleteUser($id);
	}

	/**
    * 更改自己用户信息
    * @param  Request $request [description]
    * @return [type]           [description]
    */
	public function updateOneself(Request $request){
		$data = $request->all();

		$id = Auth::user()->id;

		$oneSelfInfo = [
			'password' => bcrypt($data['newPassword']),
		];

		UserManager::updateUser($id,$oneSelfInfo);
	}

	/**
    * 更改用户所有信息
    * @param  Request $request [description]
    * @return [type]           [description]
    */
	public function updateUser(Request $request){
		$data = $request->all();

		$id = $data['id'];

		UserManager::updateUser($id,$data);
	}
}