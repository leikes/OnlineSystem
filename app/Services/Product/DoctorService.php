<?php namespace App\Services\Product;

use App\Facades\Infra\AdminManager;
use Illuminate\Http\Request;
use App\Facades\Utils\ValidateParameter;
use Auth;
class DoctorService{

	public function getAll(Request $request){

		$conditions = $request->all();

		unset($conditions['id']);
		
		return AdminManager::getDoctorByConditions($conditions);
	}

	public function createDoctor(Request $request){

		$data = $request->all();

		if(Auth::user()->entrance_id==1){
			$rules = [
				'entrance_id' => 'required|exists:entrances,id',
			];

			ValidateParameter::validate($request,$rules);
		}

		else{
			$data['entrance_id'] = Auth::user()->entrance_id;
		}

		AdminManager::createDoctor($data);
	}

	public function updateDoctor(Request $request){

		$data = $request->all();

		$id = $data['id'];

		AdminManager::updateDoctor($id,$data);
	}

	public function deleteDoctor(Request $request){
		$id = $request->id;

		AdminManager::deleteDoctor($id);
	}
}