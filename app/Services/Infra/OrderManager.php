<?php

namespace App\Services\Infra;

use App\Models\Order;
use App\Services\Traits\ModelOperatorTrait;
use App\Facades\Utils\OrderNumber;

class OrderManager{

	use ModelOperatorTrait;

	public function getAllByConditions($conditions = null)
	{
		return $this->findModel(Order::class,Order::with('province','city','entrance'),$conditions,$first = true,$like = true);
	}

	public function getAllByConditionsNotLike($conditions = null,$query){
		return $this->findModel(Order::class,$query,$conditions,$first = true);
	}

	public function getById($id){
		return Order::with('province','city','entrance')->find($id);
	}

	public function createOrder($data){
		$this->create(Order::class,$data);
		OrderNumber::setOrderNumber();
	}

	public function deleteOrder($id)
	{
		Order::find($id)->delete();
	}

	public function updateOrder($id,$data){
		$this->update(Order::class,$id,$data);
	}

	public function export($conditions = null){
		
		return $this->findModel(Order::class,Order::with('province','city','entrance'),$conditions,$first = true,$like = true)->orderBy('created_at','desc')->get();
	}
}