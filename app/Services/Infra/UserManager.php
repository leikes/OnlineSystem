<?php
namespace App\Services\Infra;

use App\Models\User;
use App\Models\Role;
use App\Services\Traits\ModelOperatorTrait;

class UserManager{

	use ModelOperatorTrait;

	/**
	 * 根据条件获取全部用户数据
	 * @param  array $conditions 条件
	 * @return result            数据结果集
	 */
	public function getAllByConditions($conditions = null){
		
		return $this->findModel(User::class, User::query(), $conditions, $first = true)->paginate(15);
	}

	public function createUser($data){
		$this->create(User::class,$data);
	}

	public function deleteUser($id){
		User::find($id)->delete();
	}

	public function updateUser($id,$data){
		$this->update(User::class,$id,$data);
	}

	public function getAllRoles($conditions){
		return $this->findModelByNot(Role::class,Role::query(),$conditions)->get();
	}
}