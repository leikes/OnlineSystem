<?php namespace App\Services\Infra;

use App\Models\Entrance;
use App\Services\Traits\ModelOperatorTrait;

class EntranceManager{

	use ModelOperatorTrait;

	public function getAllByConditions($conditions = null){

		return $this->findModel(Entrance::class, Entrance::query(),$conditions,$first = true)->paginate(15);
	}

	public function createEntrance($data){
		$this->create(Entrance::class,$data);
	}

	public function deleteEntrance($id){
		Entrance::find($id)->delete();
	}

	public function updateEntrance($id,$data){
		$this->update(Entrance::class,$id,$data);
	}
}