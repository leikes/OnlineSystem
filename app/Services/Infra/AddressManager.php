<?php namespace App\Services\Infra;

use App\Services\Traits\ModelOperatorTrait;
use App\Models\Province;
use App\Models\City;
class AddressManager{
	use ModelOperatorTrait;
	
	public function getAllProvince($conditions = null)
	{
		return $this->findModel(Province::class,Province::query(),$conditions,$first = true) ->get();
	}

	public function getAllCityByProvinceId($conditions = null)
	{
		return $this->findModel(City::class,City::query(),$conditions,$first = true) ->get();
	}
}