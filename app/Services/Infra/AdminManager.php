<?php namespace App\Services\Infra;

use App\Models\Doctor;
use App\Models\Register;
use App\Models\Disease;
use App\Models\Channel;
use App\Services\Traits\ModelOperatorTrait;

class AdminManager{

	use ModelOperatorTrait;

	public function getDoctorByConditions($conditions = null){
		return $this->findModel(Doctor::class,Doctor::query(),$conditions,$first = true)->paginate(15);
	}

	public function createDoctor($data){
		$this->create(Doctor::class,$data);
	}

	public function updateDoctor($id,$data){
		$this->update(Doctor::class,$id,$data);
	}

	public function deleteDoctor($id){
		Doctor::find($id)->delete();
	}



	public function getRegisterByConditions($conditions = null){
		return $this->findModel(Register::class,Register::query(),$conditions,$first = true)->paginate(15);
	}

	public function createRegister($data){
		$this->create(Register::class,$data);
	}

	public function updateRegister($id,$data){
		$this->update(Register::class,$id,$data);
	}

	public function deleteRegister($id){
		Register::find($id)->delete();
	}



	public function getDiseaseByConditions($conditions = null){
		return $this->findModel(Disease::class,Disease::query(),$conditions,$first = true)->paginate(15);
	}

	public function createDisease($data){
		$this->create(Disease::class,$data);
	}

	public function updateDisease($id,$data){
		$this->update(Disease::class,$id,$data);
	}

	public function deleteDisease($id){
		Disease::find($id)->delete();
	}


	public function getChannelByConditions($conditions = null){
		return $this->findModel(Channel::class,Channel::query(),$conditions,$first = true)->paginate(15);
	}

	public function createChannel($data){
		$this->create(Channel::class,$data);
	}

	public function updateChannel($id,$data){
		$this->update(Channel::class,$id,$data);
	}

	public function deleteChannel($id){
		Channel::find($id)->delete();
	}
}