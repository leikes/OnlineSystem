<?php namespace App\Services\Utils;

use App\Exceptions\BaseException;
use Excel;

class ExportExcel
{
	public function export($tableName,$data)
	{	
		if(count($data)<1){
			throw new BaseException('无导出数据');
		}

        $tableName=$tableName.'-'.strftime('%Y-%m-%d %H-%M-%S');

	  	Excel::create($tableName,function($excel) use($data){

            $excel->sheet('First sheet', function($sheet) use($data){

                $sheet->setFontSize(16);

                $sheet->setWidth(array(
                    'A'     =>  9,
                    'B'     =>  18,
                    'C'     =>  16,
                    'D'     =>  13,
                    'E'     =>  10,
                    'F'     =>  10,
                    'G'     =>  13,
                    'H'     =>  13,
                    'I'     =>  13,
                    'J'     =>  13,
                    'K'     =>  24
                ));

                $sheet->freezeFirstRow();

                $sheet->cell('A:K',function($cells)
                    {
                        $cells->setAlignment('center');
                    });

                 $sheet->cell('1',function($cells)
                    {
                        $cells->setAlignment('center');
                        $cells->setFontColor('#ffffff');
                        $cells->setBackground('#e78012');
                    });

                $sheet->setBorder('A1:k1', 'thin');

                $sheet->fromArray($data);

                
            });
        })->export('xls');
	}
}