<?php 
namespace App\Services\Utils;
use Storage;
use App\Models\Order;
class OrderNumber{
	public function getOrderNumber(){
		
		if(!Storage::has("orderNumber.txt") ||Storage::get("orderNumber.txt") == ""){
			$this->crateOrderNumber();
		}

		return Storage::get("orderNumber.txt");
	}

	public function setOrderNumber(){

		if(!Storage::has("orderNumber.txt") ||Storage::get("orderNumber.txt") == ""){
			$this->crateOrderNumber();
		}

		$startOrderNumber = Storage::get("orderNumber.txt");

		$startOrderNumber++;

		Storage::put("orderNumber.txt",$startOrderNumber);
	}

	private function crateOrderNumber(){
		$orderNumber = Order::orderBy('created_at','desc')->first();

		if($orderNumber == ""){
			$orderNumber = "00001";
		}
		else{
			$orderNumber=$orderNumber->toArray()['order_number'];
		}

		Storage::put("orderNumber.txt",$orderNumber);
	}
}