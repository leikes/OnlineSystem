<?php namespace App\Services\Utils;
use App\Http\Requests;
use App\Exceptions\ValidateParameterException;
use Validator;
class ValidateParameter
{
	public function validate($request,$rules)
	{
		$validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            throw new ValidateParameterException($validator->errors()->first());
        }

	}
}
?>