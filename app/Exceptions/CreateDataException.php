<?php namespace App\Exceptions;
use Exception;
use Config;

class CreateDataException  extends BaseException{
	public function __construct($message = null){
        $exception = Config::get('exceptions.data.create');
        if(is_null($message)){
            $message = $exception['message'];
        }
        parent::__construct($message, $exception['errCode']);
    }
}