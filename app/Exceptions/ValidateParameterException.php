<?php namespace App\Exceptions;
use Exception;
use Config;

class ValidateParameterException  extends BaseException{
	public function __construct($message = null){
        $exception = Config::get('exceptions.verification.parameterInvalid');
        if(is_null($message)){
            $message = $exception['message'];
        }
        parent::__construct($message, $exception['errCode']);
    }
}