<?php namespace App\Exceptions;
use Exception;
use JsonSerializable;

class BaseException extends  Exception implements  JsonSerializable{
	public function __construct($exception){
        if(isset($exception['message']) && isset($exception['errCode'])){
            if(is_null($exception['message']) && (($defaultConfig = $this->getDefaultConfig()) != null)){
                parent::__construct($defaultConfig['message'], $defaultConfig['errCode']);
            }else{
               parent::__construct($exception['message'], $exception['errCode']);
            }
        }
        else
        {
            if(is_null($exception) && (($defaultConfig = $this->getDefaultConfig()) != null)){
                parent::__construct($defaultConfig['message'], $defaultConfig['errCode']);
            }else{
               parent::__construct($exception,10001);
            }
        }
    }

    public function getDefaultConfig(){
        return !isset($this->defaultConfig) ?: config($this->defaultConfig);
    }

    public function jsonSerialize(){

        return [
            'status' => [
                'errCode' => $this->getCode(),
                'message' => $this->getMessage()
            ],
        ];
    }
}
