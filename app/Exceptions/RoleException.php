<?php namespace App\Exceptions;
use Exception;
use Config;

class RoleException  extends BaseException{
	public function __construct($message = null){
        $exception = Config::get('exceptions.role.role');
        if(is_null($message)){
            $message = $exception['message'];
        }
        parent::__construct($message, $exception['errCode']);
    }
}