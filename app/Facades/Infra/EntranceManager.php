<?php namespace App\Facades\Infra;
use Illuminate\Support\Facades\Facade;
class EntranceManager extends Facade
{
	protected static function getFacadeAccessor()
	{
		return 'App\Services\Infra\EntranceManager';
	}
}

?>