<?php namespace App\Facades\Infra;
use Illuminate\Support\Facades\Facade;
class OrderManager extends Facade
{
	protected static function getFacadeAccessor()
	{
		return 'App\Services\Infra\OrderManager';
	}
}

?>