<?php namespace App\Facades\Infra;
use Illuminate\Support\Facades\Facade;
class AddressManager extends Facade
{
	protected static function getFacadeAccessor()
	{
		return 'App\Services\Infra\AddressManager';
	}
}

?>