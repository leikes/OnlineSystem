<?php namespace App\Facades\Product;
use Illuminate\Support\Facades\Facade;
class OrderService extends Facade
{
	protected static function getFacadeAccessor()
	{
		return 'App\Services\Product\OrderService';
	}
}

?>