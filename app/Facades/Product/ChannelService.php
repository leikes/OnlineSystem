<?php namespace App\Facades\Product;
use Illuminate\Support\Facades\Facade;
class ChannelService extends Facade
{
	protected static function getFacadeAccessor()
	{
		return 'App\Services\Product\ChannelService';
	}
}

?>