<?php namespace App\Facades\Product;
use Illuminate\Support\Facades\Facade;
class DiseaseService extends Facade
{
	protected static function getFacadeAccessor()
	{
		return 'App\Services\Product\DiseaseService';
	}
}

?>