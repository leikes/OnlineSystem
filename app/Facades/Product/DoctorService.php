<?php namespace App\Facades\Product;
use Illuminate\Support\Facades\Facade;
class DoctorService extends Facade
{
	protected static function getFacadeAccessor()
	{
		return 'App\Services\Product\DoctorService';
	}
}

?>