<?php namespace App\Facades\Product;
use Illuminate\Support\Facades\Facade;
class AddressService extends Facade
{
	protected static function getFacadeAccessor()
	{
		return 'App\Services\Product\AddressService';
	}
}

?>