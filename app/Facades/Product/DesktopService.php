<?php namespace App\Facades\Product;
use Illuminate\Support\Facades\Facade;
class DesktopService extends Facade
{
	protected static function getFacadeAccessor()
	{
		return 'App\Services\Product\DesktopService';
	}
}

?>