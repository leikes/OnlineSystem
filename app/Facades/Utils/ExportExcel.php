<?php namespace App\Facades\Utils;

use Illuminate\Support\Facades\Facade;

class ExportExcel extends Facade
{
	protected static function getFacadeAccessor()
	{
		return 'App\Services\Utils\ExportExcel';
	}
}

?>