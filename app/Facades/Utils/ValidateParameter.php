<?php namespace App\Facades\Utils;
use Illuminate\Support\Facades\Facade;
class ValidateParameter extends Facade
{
	protected static function getFacadeAccessor()
	{
		return 'App\Services\Utils\ValidateParameter';
	}
}

?>