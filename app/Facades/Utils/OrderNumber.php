<?php namespace App\Facades\Utils;

use Illuminate\Support\Facades\Facade;

class OrderNumber extends Facade
{
	protected static function getFacadeAccessor()
	{
		return 'App\Services\Utils\OrderNumber';
	}
}

?>