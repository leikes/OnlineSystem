<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MailInfo extends Model
{
    protected $table ='mail_infos'

    protected $fillable = [
    	'name',
    	'sex',
    	'phone',
    	'age',
    	'doctor',
    	'desc',
    	'entrance_id'
    ];
}
