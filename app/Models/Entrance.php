<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Entrance extends Model
{
    protected $table = 'entrances';

    protected $fillable = [
    	'id',
    	'entrance_name'
    ];
}
