<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    const READY_PASS = 1;
    const PEADY_WAIT = 0;

    const SEX_PASS = 1;
    const SEX_WAIT =0;

    public static $READY=[
        self::READY_PASS => '已到诊',
        self::PEADY_WAIT => '未到诊'
    ];

    public static $SEX=[
        self::SEX_PASS => '男',
        self::SEX_WAIT => '女'
    ];

    protected $table = 'orders';

    protected $fillable = [
    	'name',
    	'phone',
    	'age',
    	'sex',
    	'order_date',
        'arrival_date',
    	'register_type',
    	'order_number',
    	'is_ready',
    	'doctor',
    	'user_name',
    	'disease_type',
    	'province_id',
    	'city_id',
        'address',
    	'channel',
    	'keyword',
    	'remarks',
    	'record',
    	'visit_date',
    	'visit_info',
        'qq',
        'wechat',
        'clinical',
    	'entrance_id'
    ];

     protected $appends=[
        'ready_desc',
        'sex_desc'
    ];

    public function getReadyDescAttribute(){
        if(array_key_exists('is_ready',$this->attributes)){
            return $this->attributes['ready_desc']=self::$READY[$this->attributes['is_ready']];
        }
    }

    public function getSexDescAttribute(){
        if(array_key_exists('sex',$this->attributes)){
            return $this->attributes['sex_desc']=self::$SEX[$this->attributes['sex']];
        }
    }

    public function entrance()
    {
        return $this->belongsTo('App\Models\Entrance','entrance_id');
    }

    public function province()
    {
        return $this->belongsTo('App\Models\Province','province_id');
    }

    public function city()
    {
        return $this->belongsTo('App\Models\City','city_id');
    }
}
