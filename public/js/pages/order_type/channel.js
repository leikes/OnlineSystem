(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
$(function() {
  $(".update_a").click(function(e) {
    var param;
    param = {
      id: $(e.target).parent().parent().children(".id").html(),
      channel_name: $(e.target).parent().parent().children(".name").children("input").val()
    };
    return $.post('/api/channel/update', param, function(res) {
      if (res.status.errCode === 0) {
        alert("修改成功");
        return location.reload();
      } else {
        return alert(res.status.message);
      }
    });
  });
  $(".delete_a").click(function(e) {
    var param;
    param = {
      id: $(e.target).parent().parent().children(".id").html()
    };
    return $.post('/api/channel/delete', param, function(res) {
      if (res.status.errCode === 0) {
        alert("删除成功");
        return location.reload();
      } else {
        return alert(res.status.message);
      }
    });
  });
  return $("#create_btn").click(function(e) {
    var param;
    param = {
      channel_name: $("#channel_name").val()
    };
    return $.post('/api/channel/create', param, function(res) {
      if (res.status.errCode === 0) {
        alert("添加成功");
        return location.reload();
      } else {
        return alert(res.status.message);
      }
    });
  });
});

},{}]},{},[1]);
