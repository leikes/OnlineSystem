(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
$(function() {
  var key, pageUrl, value;
  pageUrl = {
    orders: "/order/list",
    orders_create: "/order/create",
    order_type: "/order-type/list",
    users: "/user/list",
    user_create: "/user/create",
    desktop: "/desktop/index"
  };
  for (key in pageUrl) {
    value = pageUrl[key];
    if (window.location.pathname === value) {
      $("#" + key).addClass("click-now");
    }
  }
  $(".child").click(function(e) {
    var jumpUrl;
    for (key in pageUrl) {
      value = pageUrl[key];
      if (key === this.id) {
        jumpUrl = key;
        break;
      }
    }
    return window.location.href = pageUrl[jumpUrl];
  });
  return $(".nav_box").height($(document).height());
});

},{}]},{},[1]);
