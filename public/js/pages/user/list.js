(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
$(function() {
  $(".update_a").click(function(e) {
    var param;
    param = {
      id: $(e.target).parent().parent().children(".id").html(),
      name: $(e.target).parent().parent().children(".name").children("input").val(),
      uname: $(e.target).parent().parent().children(".uname").children("input").val(),
      role_id: $(e.target).parent().parent().children(".role").children("select").val()
    };
    return $.post("/api/user/update-user", param, function(res) {
      if (res.status.errCode === 0) {
        alert("修改成功");
        return window.location.reload();
      } else {
        return alert(res.status.message);
      }
    });
  });
  return $(".delete_a").click(function(e) {
    var param;
    param = {
      id: $(e.target).parent().parent().children(".id").html()
    };
    return $.post("/api/user/delete", param, function(res) {
      if (confirm("确认删除吗？") === false) {

      } else {
        if (res.status.errCode === 0) {
          alert("修改成功");
          return window.location.reload();
        } else {
          return alert(res.status.message);
        }
      }
    });
  });
});

},{}]},{},[1]);
