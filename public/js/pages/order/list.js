(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
$(function() {
  var Address, City, IsReady, Province, Search, VisitDate;
  Search = function() {
    if ($("#search_input").val() !== "") {
      return "&" + $("#search_select").val() + "=" + $("#search_input").val();
    } else {
      return "&" + $("#search_select").val() + "=";
    }
  };
  IsReady = function() {
    if ($("#is_ready").val() !== "") {
      return "&is_ready=" + $("#is_ready").val();
    } else {
      return "";
    }
  };
  VisitDate = function() {
    if ($("#visit_date").val() !== "") {
      return "&visit_date=" + $("#visit_date").val();
    } else {
      return "";
    }
  };
  Province = function() {
    if ($("#province_id").val() !== "") {
      return "&province_id=" + $("#province_id").val();
    } else {
      return "";
    }
  };
  City = function() {
    if ($("#city_id").val() !== "") {
      return "&city_id=" + $("#city_id").val();
    } else {
      return "";
    }
  };
  Address = function() {
    if ($("#address").val() !== "") {
      return "&address=" + $("#address").val();
    } else {
      return "";
    }
  };
  $("#province_id").change(function(e) {
    var param;
    param = {
      'province_id': $("#province_id").val()
    };
    return $.get('/api/address/get-city-province-id', param, function(res) {
      var city, i, len, ref, result;
      result = "<option value=''>--请选择--</option>";
      ref = res.data;
      for (i = 0, len = ref.length; i < len; i++) {
        city = ref[i];
        result = result + "<option value='" + city.id + "'>" + city.city_name + "</option>";
      }
      $("#city_id").empty();
      return $("#city_id").append(result);
    });
  });
  $(".sech_btn").click(function(e) {
    return window.location.href = "/order/list?" + Search() + IsReady() + VisitDate() + Province() + City() + Address();
  });
  $("#is_ready").change(function(e) {
    return window.location.href = "/order/list?" + Search() + IsReady() + VisitDate() + Province() + City() + Address();
  });
  $("#visit_date").blur(function(e) {
    return window.location.href = "/order/list?" + Search() + IsReady() + VisitDate() + Province() + City() + Address();
  });
  $("#pagination a").each(function() {
    var zuiJiaHref;
    zuiJiaHref = $(this).attr('href');
    return $(this).attr('href', zuiJiaHref + Search() + IsReady() + VisitDate() + Province() + City() + Address());
  });
  $("#prePagination a").each(function() {
    var zuiJiaHref;
    if ($("#previous_page_url").val() !== "") {
      zuiJiaHref = $(this).attr('href');
      return $(this).attr('href', zuiJiaHref + Search() + IsReady() + VisitDate() + Province() + City() + Address());
    }
  });
  $("#nextPagination a").each(function() {
    var zuiJiaHref;
    if ($("#next_page_url").val() !== "") {
      zuiJiaHref = $(this).attr('href');
      return $(this).attr('href', zuiJiaHref + Search() + IsReady() + VisitDate() + Province() + City() + Address());
    }
  });
  return $("#export_btn").click(function(e) {
    if ($("#list_content").children().length === 0) {
      return alert("无数据导出");
    } else {
      return window.location.href = "/api/order/export?" + Search() + IsReady() + VisitDate() + Province() + City() + Address();
    }
  });
});

},{}]},{},[1]);
