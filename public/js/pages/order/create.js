(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
$(function() {
  $(".confirm_btn").click(function(e) {
    var param;
    param = {
      name: $("#name").val(),
      phone: $("#phone").val(),
      age: $("#age").val(),
      sex: $("#sex").val(),
      order_date: $("#order_date").val(),
      arrival_date: $("#arrival_date").val(),
      register_type: $("#register_type").val(),
      order_number: $("#order_number").val(),
      is_ready: $("#is_ready").val(),
      doctor: $("#doctor").val(),
      disease_type: $("#disease_type").val(),
      channel: $("#channel").val(),
      province_id: $("#province").val(),
      city_id: $("#city").val(),
      address: $("#address").val(),
      keyword: $("#keyword").val(),
      visit_date: $("#visit_date").val(),
      visit_info: $("#visit_info").val(),
      remarks: $("#remarks").val(),
      record: $("#record").val(),
      qq: $("#qq").val(),
      wechat: $("#wechat").val(),
      clinical: $("#clinical").val()
    };
    return $.post('/api/order/create', param, function(res) {
      if (res.status.errCode === 0) {
        alert("新建成功");
        return window.location.href = '/order/list';
      } else {
        return alert(res.status.message);
      }
    });
  });
  $("#province").change(function(e) {
    var param;
    param = {
      'province_id': $("#province").val()
    };
    return $.get('/api/address/get-city-province-id', param, function(res) {
      var city, i, len, ref, result;
      result = [];
      ref = res.data;
      for (i = 0, len = ref.length; i < len; i++) {
        city = ref[i];
        result = result + "<option value='" + city.id + "'>" + city.city_name + "</option>";
      }
      return $("#city").html(result);
    });
  });
  return $(".cancel_btn").click(function(e) {
    history.go(-1);
    return location.reload();
  });
});

},{}]},{},[1]);
