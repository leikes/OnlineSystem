(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
$(function() {
  $("#update_btn").click(function(e) {
    if ($("#update_status").val() === "2" || $("#update_status").val() === "1") {
      return window.location.href = "/order/update?id=" + $("#order_id").val();
    } else {
      return alert("无修改权限");
    }
  });
  return $("#cancel_btn").click(function(e) {
    return window.location.href = "/order/list";
  });
});

},{}]},{},[1]);
